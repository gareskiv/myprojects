
let balance = 0;
let budget = 0;
let expenses = 0;
let globalIndex;

function submitBudget(e) {
    e.preventDefault();
    
    let yourBalance = document.getElementById('budget-input');
    if (yourBalance.value === "" || yourBalance.value <= 0) {
        let invalid = document.getElementById('invalid-form');
        invalid.innerHTML = 'Value cannot be empty or negative.';
        invalid.style.display = 'block';
        return;
    }     
    
    let tmp = parseInt(yourBalance.value);
    if (!isNaN(tmp) && tmp >= 0) {
        budget = tmp;
    }

    let budgetHtml = document.getElementById('budget-amount');
    let balanceHtml = document.getElementById('balance-amount');
    let expenseHtml = document.getElementById('expense-amount');
        
    budgetHtml.innerHTML = budget;
    balanceHtml.innerHTML = (budget - expenses);
    expenseHtml.innerHTML = expenses;
    
    yourBalance.value = "";
}

document.getElementById('budget-submit').addEventListener('click', submitBudget);
document.getElementById('budget-input').addEventListener('focus', function() {
    document.getElementById('invalid-form').style.display = '';
});


function submitExpenses(e) {
    e.preventDefault();

    let yourExpensesTitle = document.getElementById('expense-input');
    let yourExpenses = document.getElementById('amount-input');
    if (yourExpenses.value === "" || yourExpenses.value <= 0 || yourExpensesTitle.value === "") {
        let invalid = document.getElementById('invalid-expenses');
        invalid.innerHTML = 'Value cannot be empty or negative.';
        invalid.style.display ='block';
        return;
    }


    let tmp = parseInt(yourExpenses.value);
    if (!isNaN(tmp) && tmp >= 0) {
        expenses += tmp;
    }

    let expenseHtml = document.getElementById('expense-amount');
    let balanceHtml = document.getElementById('balance-amount'); 
    let theTable = document.getElementById('myTable');
    theTable.style.display = 'inline-table';

    if (globalIndex) {
        let tmpTitle = yourExpensesTitle.value;
        let tmpValue = yourExpenses.value;
        let editIndexRow = theTable.rows[globalIndex].cells[0].innerHTML;       
        let editIndexRow1 = parseInt(theTable.rows[globalIndex].cells[1].innerHTML);

        expenses -= editIndexRow1;
        
        yourExpensesTitle.value = editIndexRow.substr(2);        
        yourExpenses.value = editIndexRow1;


        balanceHtml.innerHTML = (budget - expenses); 
        expenseHtml.innerHTML = expenses;

       
        theTable.rows[globalIndex].cells[0].innerHTML = '- ' + tmpTitle.toUpperCase();        
        theTable.rows[globalIndex].cells[1].innerHTML = tmpValue;

        yourExpensesTitle.value = "";
        yourExpenses.value = "";
        
        globalIndex = null;
        return;
    }

    balanceHtml.innerHTML = (budget - expenses); 
    expenseHtml.innerHTML = expenses;

    let row = theTable.insertRow();
    row.innerHTML = `<td class="expense-title">- ${yourExpensesTitle.value.toUpperCase()}</td>
                    <td class="expense-amount">${yourExpenses.value}</td>`;

    let col = document.createElement('td');
    
    let editBtn = document.createElement('a');
    editBtn.innerHTML = '<i class="far fa-edit edit-icon"></i>';
    col.appendChild(editBtn);
    editBtn.addEventListener('click', function(e){
        e.preventDefault();        
        let editIndex = e.target.parentElement.parentElement.parentElement.rowIndex;
        globalIndex = editIndex;
        let editIndexRow = theTable.rows[editIndex].cells[0].innerHTML;    
        let editIndexRow1 = parseInt(theTable.rows[editIndex].cells[1].innerHTML);

        yourExpensesTitle.value = editIndexRow.substr(2).toLowerCase();        
        yourExpenses.value = editIndexRow1;
    });
    
    let deleteBtn = document.createElement('a');
    deleteBtn.innerHTML = `<i class="fas fa-trash-alt delete-icon"></i>`;
    deleteBtn.addEventListener('click', function(e) {
        e.preventDefault();
        
        let rowToRemove = e.target.parentElement.parentElement.parentElement;
        let rowIndexToRemove = e.target.parentElement.parentElement.parentElement.rowIndex;
        let expenseToRemove = parseInt(theTable.rows[rowIndexToRemove].cells[1].innerHTML);
        rowToRemove.remove();

        if (globalIndex > 0 && globalIndex > rowIndexToRemove) {
            globalIndex--;
        }
        
        expenses -= expenseToRemove;
        balanceHtml.innerHTML = (budget - expenses); 
        expenseHtml.innerHTML = expenses;

        if (theTable.rows.length === 1) {
            theTable.style.display = 'none';
        }
    });
    col.appendChild(deleteBtn);

    row.appendChild(col);

    yourExpensesTitle.value = "";
    yourExpenses.value = "";
}


document.getElementById('expense-submit').addEventListener('click', submitExpenses);

document.getElementById('expense-input').addEventListener('focus', function() {
    document.getElementById('invalid-expenses').style.display = '';
});

document.getElementById('amount-input').addEventListener('focus', function() {
    document.getElementById('invalid-expenses').style.display = '';
});

