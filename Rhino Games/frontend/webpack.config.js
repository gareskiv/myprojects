const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ESLintPlugin = require('eslint-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = ({ mode } = { mode: "production" }) => {
    console.log(`mode is: ${mode}`);

    return {
        mode,
        entry: "./src/index.js",
        devServer: {
            hot: true,
            open: true,
            historyApiFallback: true,
        },
        output: {
            publicPath: "/",
            path: path.resolve(__dirname, "build"),
            filename: "bundle.js"
        },
        module: {
            rules: [ 
                {
                    test: /\.(png|jpg|jpeg|gif|svg|avif)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: './public/images/',
                            },
                        },
                    ],
                },
                {
                    test: /\.css$/i,
                    use: ["style-loader", "css-loader"],
                },               
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                      "style-loader",
                      "css-loader",
                      {
                        loader: "sass-loader",
                        options: {
                          implementation: require("sass"),
                        },
                      },
                    ],
                },                
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use:[
                        {
                            loader: "babel-loader",
                        },                    
                    ]                    
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: "./public/index.html"
            }),
            new ESLintPlugin({
                fix: true
            }),
            new StyleLintPlugin({
                files: ['**/*.css', '**/*.scss'], 
                failOnError: false,
                quiet: false,  
                fix: true,
            }),
        ]
    }

};