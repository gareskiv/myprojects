export const updateSearchInput = (searchInput) => {
  return {
    type: 'UPDATE_SEARCH_INPUT',
    payload: searchInput
  }
}

export const clearSearchInput = () => {
  return {
    type: 'CLEAR_SEARCH_INPUT'
  }
}

export const loggedUserData = (data) => {
  return {
    type: 'LOGGED_USER_DATA',
    payload: data
  }
}

export const clearLoggedUserData = () => {
  return {
    type: 'CLEAR_LOGGED_USER_DATA'
  }
}

export const setActiveLink = (link) => {
  return {
    type: 'SET_ACTIVE_LINK',
    link
  }
}

export const clearActiveLink = () => {
  return {
    type: 'CLEAR_ACTIVE_LINK'
  }
}
