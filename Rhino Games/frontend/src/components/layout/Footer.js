import moment from 'moment'

export default function Footer () {
  const getMoment = moment()

  return <footer className="text-center bg-dark text-white">
        <div className="footer-content">
            <div>
                Created by <b className="text-danger">Viktor Gareski</b>
            </div>
            <div>
                © {getMoment.year()}
            </div>
        </div>
    </footer>
}
