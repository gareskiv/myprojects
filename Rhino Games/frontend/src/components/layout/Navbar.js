import { useState } from 'react'
import {
  Container,
  Nav,
  Navbar,
  Form,
  Button,
  NavDropdown
} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { useSelector, useDispatch, batch } from 'react-redux'
import {
  updateSearchInput,
  setActiveLink,
  clearLoggedUserData,
  clearSearchInput,
  clearActiveLink
} from '../../actions'
import { Link, useNavigate } from 'react-router-dom'
import axios from 'axios'

export default function Navigator () {
  const host = 'http://localhost:8080'
  const loggedUser = useSelector(state => state.loggedUser.data)
  const searcBarValue = useSelector(state => state.updateSearchInput.searchInput)
  const [searchBar, setSearchBar] = useState(searcBarValue)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const searchBarHanlder = (e) => {
    // convert searchbar text to lower case
    const lowerCase = e.target.value.toLowerCase()
    setSearchBar(lowerCase)
    dispatch(updateSearchInput(lowerCase))
  }

  const linkClick = (link) => {
    dispatch(setActiveLink(link))
  }
  const activeLink = useSelector(state => state.setActiveLink)
  const activeLinkClass = location.pathname === activeLink.activeLink ? 'nav-link-active' : ''

  const goToLogin = () => {
    return navigate('/login')
  }

  const goToSettings = () => {
    return navigate('/settings')
  }

  const logout = async () => {
    await axios.get(`${host}/api/v1/users/auth/logout`, { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
      .then(response => {
        batch(() => {
          dispatch(clearLoggedUserData())
          dispatch(clearSearchInput())
          dispatch(clearActiveLink())
        })
        setSearchBar('')
      }).catch(error => {
        console.log('Something went wrong.')
      })
  }

  return <Navbar className="navigator" bg="dark" variant="dark" expand="lg" fixed="top">
            <Container className="navigator-container">
                <div className="brand-toggle-container flex-fill">
                    <Navbar.Toggle className="nav-custom-toggle" aria-controls="basic-navbar-nav" />
                    <Navbar.Brand className="m-0">
                        <Link to="/" replace={true} className="custom-brand-link">
                            <span>
                                <span className="text-danger">R</span>eact <span className="text-warning">G</span>ames
                            </span>
                        </Link>
                    </Navbar.Brand>
                    {!loggedUser.isLogged && location.pathname !== '/login' && location.pathname !== '/signup'
                      ? (
                        <Button className="login-btn" onClick={goToLogin} variant="outline-warning">Login</Button>)
                      : ''}
                </div>
                {location.pathname !== '/login' && location.pathname !== '/signup'
                  ? (
                <div className="searchbar-container">
                    <Form className="d-flex align-items-center searchbar mx-1 my-2">
                        <Form.Control
                            type="text"
                            placeholder="Search Games"
                            className="form-input"
                            aria-label="Search"
                            aria-describedby="forGroupPrepend"
                            value={searchBar}
                            onChange={searchBarHanlder}
                        />
                        <FontAwesomeIcon icon={faSearch} className="text-muted input-icon" size="lg" />
                    </Form>
                </div>)
                  : ''}
                <div className="collapse-container">
                    <Navbar.Collapse id="basic-navbar-nav" className="custom-style-collapse">
                        <Nav className="mx-1 my-2">
                            <Nav.Link
                                as={Link}
                                to="/" // insert route name here
                                onClick={() => linkClick('/')}
                                className={activeLink.activeLink === '/' ? activeLinkClass : ''}
                            > Game Browser </Nav.Link>
                            {loggedUser.isLogged
                              ? (<div className="custom-dropdown">
                                <NavDropdown className="custom-nav-dropdown" title={loggedUser.username} id="username-dropdown">
                                    <NavDropdown.Item className="custom-nav-item" onClick={goToSettings}>
                                        Settings
                                    </NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item className="custom-nav-item" onClick={logout}>
                                        Logout
                                    </NavDropdown.Item>
                                </NavDropdown>
                            </div>)
                              : ''}
                        </Nav>
                    </Navbar.Collapse>
                </div>
            </Container>
    </Navbar>
}
