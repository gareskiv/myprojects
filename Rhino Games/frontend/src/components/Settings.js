import moment from 'moment'
import { Form, Button, Card, Alert } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { useState } from 'react'
import axios from 'axios'
import { loggedUserData } from '../actions'
import { ThreeDots } from 'react-loader-spinner'

export default function Settings () {
  const host = 'http://localhost:8080'
  const dispatch = useDispatch()
  const date = moment()
  const currentHour = date.hours()
  const currentUser = useSelector(state => state.loggedUser.data)
  const [username, setUsername] = useState(currentUser.username)
  const [showLoader, setLoader] = useState(false) // this is to simulate loader when data is send to the server
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)

  const handleSettings = async (e) => {
    e.preventDefault()

    if (username && username != '') {
      const newUserObject = {
        username: e.target.elements.username.value
      }

      if (newUserObject.username === currentUser.username) return

      setError('')
      setLoader(true)

      await axios.post(`${host}/api/v1/settings/${currentUser.id}`, newUserObject, { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
        .then(response => {
          setSuccess(true)
          if (response.status == 200) {
            dispatch(loggedUserData({
              id: response.data.id,
              username: response.data.username,
              email: response.data.email,
              token: currentUser.token,
              isLogged: true
            }))
          }
          setSuccess(true)
          setTimeout(() => {
            setSuccess(false)
          }, 2500)
        }).catch(error => {
          if (String(error.response.status)[0] === '4') {
            if (error.response.data.message) {
              setError(error.response.data.message)
            } else {
              setError(error.response.data.errors[0].msg)
            }
          } else {
            setError('Something bad happened.')
          }
        }).finally(() => {
          setLoader(false)
        })
    }
  }

  const capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  let greeting = ''

  if (currentHour >= 6 && currentHour < 12) {
    greeting = 'Good morning'
  } else if (currentHour >= 12 && currentHour < 18) {
    greeting = 'Good afternoon'
  } else {
    greeting = 'Good evening'
  }

  return <div className="form-container">
            <Card className="py-4 form-card card-class">
                <div className="text-center">
                        <h2>Profile Settings</h2>
                    <h6 className="text-muted">
                        {!showLoader ? (<span>{greeting}, {capitalizeFirstLetter(currentUser.username)}!</span>) : (<span>Updating...</span>)}
                    </h6>
                </div>
                <Card.Body>
                    {error != '' ? (<Alert key="danger" variant="danger">{error}</Alert>) : ''}
                    {success ? (<Alert key="success" variant="success">Successfully updated username!</Alert>) : ''}

                    {showLoader
                      ? (
                        <div className="d-flex w-100 align-items-center justify-content-center">
                            <ThreeDots hheight="45"
                                width="45"
                                radius="9"
                                color="red"
                                ariaLabel="three-dots-loading"
                                visible={true}/>
                        </div>
                        )
                      : (
                        <Form onSubmit={handleSettings}>
                            <Form.Group className="mb-3" controlId="username">
                                <Form.Label>Username</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter username"
                                    className="me-2"
                                    aria-label="Username"
                                    value={username.toLowerCase()}
                                    onChange={e => setUsername(e.target.value)}
                                />
                            </Form.Group>
                            <div className="form-btn-container w-100">
                                <Button variant="dark" type="submit" className="form-btn">
                                    Change
                                </Button>
                            </div>
                        </Form>)}
                </Card.Body>
            </Card>
        </div>
}
