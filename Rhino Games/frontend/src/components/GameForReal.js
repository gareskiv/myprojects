import { useParams } from 'react-router-dom'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { Container, Form, Button, Row, Col, InputGroup } from 'react-bootstrap'
import { io } from 'socket.io-client'

const socket = io('localhost:8080', {
  upgrade: false,
  transports: ['websocket']
})

export default function GameForReal () {
  const { id } = useParams()
  const host = 'http://localhost:8080'
  const [gameData, setGameData] = useState({})
  const fetchGameData = async (id) => {
    await axios.get(`${host}/api/v1/games/play/${id}`, { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
      .then(response => {
        setGameData(response.data)
      }).catch(error => {
        // console.log(error);
        console.log('Trouble getting game data.')
      })
  }
  const [userInfo, setUserInfo] = useState('')
  const [username, setUsername] = useState('')
  const [intro, setIntro] = useState('')
  const [name, setName] = useState('')
  const [msgArea, setMsgArea] = useState('')
  const [message, setMessage] = useState('')
  const [msgBoard, setMsgBoard] = useState('')
  const [msgLog, setMsgLog] = useState('')
  const square = document.querySelector('#square')
  const [squareStyleDisplay, setSquareDisplay] = useState('')
  const [squareStyleLeft, setSquareLeft] = useState('')
  const [squareStyleTop, setSquareTop] = useState('')

  const setNameHandler = (e) => {
    e.preventDefault()
    setName(e.target.value)
  }

  const setMessageHandler = (e) => {
    e.preventDefault()
    setMessage(e.target.value)
  }

  const startBtnHandle = () => {
    const nameValue = name
    if (nameValue) {
      socket.emit('start', nameValue)
      setName('')
    } else {
      alert('Please set a username')
    }
  }

  const messageBtnHandle = () => {
    const messageValue = message || ''
    const userValue = username
    if (userValue && messageValue) {
      socket.emit('chat', { user: userValue, message: messageValue })
      setMessage('')
    }
  }

  const canvasHandle = (e) => {
    const { xPosition, yPosition } = move(e)
    socket.emit('movement', { xPosition, yPosition })
  }

  socket.on('join', message => {
    setMsgLog(`${message}\n${msgLog}`)
  })

  socket.on('name', name => {
    setUsername(name)
    setIntro('none')
    setUserInfo('block')
    setMsgArea('block')
    setMsgBoard('block')
    setSquareDisplay('block')
    setIntro('none')
  })

  socket.on('chat', data => {
    setMsgLog(`${data.user} says: ${data.message}\n${msgLog}`)
  })

  socket.on('init', data => {
    setSquareLeft(`${data.xPosition}px`)
    setSquareTop(`${data.yPosition}px`)
  })

  socket.on('movement', data => {
    setSquareLeft(`${data.xPosition}px`)
    setSquareTop(`${data.yPosition}px`)
  })

  socket.on('leave', message => {
    setMsgLog(`${message}\n${msgLog}`)
  })

  // Square movement function
  const move = (e) => {
    const parentPosition = getPosition(e.target)
    const xPosition = e.clientX - parentPosition.x - (square.clientWidth / 2)
    const yPosition = e.clientY - parentPosition.y - (square.clientHeight / 2)

    return {
      xPosition,
      yPosition
    }
  }

  // Helper function to get an element's exact position
  function getPosition (el) {
    let xPos = 0
    let yPos = 0

    while (el) {
      if (el.tagName == 'BODY') {
        // deal with browser quirks with body/window/document and page scroll
        const xScroll = el.scrollLeft || document.documentElement.scrollLeft
        const yScroll = el.scrollTop || document.documentElement.scrollTop

        xPos += (el.offsetLeft - xScroll + el.clientLeft)
        yPos += (el.offsetTop - yScroll + el.clientTop)
      } else {
        // for all other non-BODY elements
        xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft)
        yPos += (el.offsetTop - el.scrollTop + el.clientTop)
      }

      el = el.offsetParent
    }

    return {
      x: xPos,
      y: yPos
    }
  }

  useEffect(() => {
    fetchGameData(id)
  }, [])

  return <Container>
        <Row>
            <div id="userinfo" style={{ display: userInfo }}>
                <h4 className="text-center">Hello <b className="text-warning" id="username">{username}</b></h4>
            </div>
            <h4 className="text-dark text-center">
                Welcome to <b className="text-danger">{gameData.title}</b> play
            </h4>
            <h5 className="text-muted text-center">- Sample demonstration for Websocket -</h5>
        </Row>
        <Row className="align-items-center justify-content-start">
            <Row className="align-items-center justify-content-center" id="intro" style={{ display: intro }}>
                <Col xs="auto">
                    <InputGroup className="mb-3">
                        <Form.Control className="username-input mb-2" id="name" type="text" placeholder="Enter nickname to start" onChange={setNameHandler}></Form.Control>
                        <Button onClick={startBtnHandle} className="game-play-btn mb-2" variant="dark" id="start" type="submit">Start</Button>
                    </InputGroup>
                </Col>
            </Row>
        </Row>
        <Row className="justify-content-start align-items-center">
            <Col id="msgarea" md="6" xs="12" style={{ display: msgArea }}>
                <InputGroup className="mb-3">
                    <Button onClick={messageBtnHandle} className="game-play-btn mb-2" variant="dark" id="send" type="submit">Send</Button>
                    <Form.Control value={message} className="username-input mb-2" id="message" type="text" name="message" placeholder="Enter your message" onChange={setMessageHandler} ></Form.Control>
                </InputGroup>
            </Col>
            <Col id="msgboard" md="6" xs="12" style={{ display: msgBoard }}>
                <Form.Control value={msgLog} id="msglog" as="textarea" className="message-textarea mb-2 py-3" name="msglog" readOnly={true} rows={4}/>
            </Col>
        </Row>
        <Row className="align-items-center mb-2 mt-2">
            <Col onClick={canvasHandle} id="canvas">
                <div style={{
                  display: squareStyleDisplay,
                  top: squareStyleTop,
                  left: squareStyleLeft,
                  backgroundImage: `url(../images/${gameData.cover})`
                }} id="square"></div>
            </Col>
        </Row>
    </Container>
}
