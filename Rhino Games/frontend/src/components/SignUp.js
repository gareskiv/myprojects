import Login from './Login'
import { Button, Form, Card, Alert } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { useState } from 'react'
import axios from 'axios'
import { ThreeDots } from 'react-loader-spinner'

export default function SignUp () {
  const host = 'http://localhost:8080'
  const [showLoader, setLoader] = useState(false)
  const [formData, setFormData] = useState({
    email: '',
    username: '',
    password: ''
  })
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)

  const checkFormData = e => {
    e.preventDefault()

    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
    setError('')
  }

  const handleFormData = e => {
    e.preventDefault()

    if (formData.email == '' && formData.username == '' && formData.password == '') return

    setLoader(true)
    setError('')

    axios.post(`${host}/api/v1/users/auth/signup`, formData, { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
      .then(() => {
        setFormData({
          email: '',
          username: '',
          password: ''
        })
        setLoader(false)
        setSuccess(true)
        setTimeout(() => {
          setSuccess(false)
        }, 2500)
      }).catch(error => {
        setLoader(false)
        if (String(error.response.status)[0] === '4') {
          if (error.response.data.message) {
            setError(error.response.data.message)
          } else {
            setError(error.response.data.errors[0].msg)
          }
        } else {
          setError('Something bad happened.')
        }
      })
  }

  return <div className='form-container'>
        <Card className="py-4 form-card card-class">
            <div className="text-center">
                <h2>Sign Up</h2>
                <h6 className="text-muted">
                    {!showLoader ? (<span>Please fill up the form!</span>) : (<span>Loading...</span>)}
                </h6>
            </div>
            <Card.Body>
                {error != '' ? (<Alert key="danger" variant="danger">{error}</Alert>) : ''}
                {success ? (<Alert key="success" variant="success">Successfully created account!</Alert>) : ''}

                {showLoader
                  ? (
                    <div className="d-flex w-100 align-items-center justify-content-center">
                        <ThreeDots hheight="45"
                            width="45"
                            radius="9"
                            color="red"
                            ariaLabel="three-dots-loading"
                            visible={true}/>
                    </div>
                    )
                  : (
                <Form onSubmit={handleFormData}>
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={formData.email} name="email" onChange={checkFormData}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="username">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Enter username" name="username" value={formData.username.toLowerCase()} onChange={checkFormData}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" name="password" value={formData.password} onChange={checkFormData}/>
                    </Form.Group>
                    <div className="form-btn-container w-100">
                        <Button variant="dark" type="submit" className="form-btn">
                            Sign Up
                        </Button>
                    </div>
                    <div className="text-center mt-3">
                        Already have an account?
                        <Link className="px-1 text-danger" to="/login" element={<Login/>}>Log In</Link>
                    </div>
                </Form>)}
            </Card.Body>
        </Card>
    </div>
}
