import GamesCard from './GamesCard'
import { Container } from 'react-bootstrap'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { ThreeDots } from 'react-loader-spinner'

export default function ListElements () {
  const host = 'http://localhost:8080'
  const [allGames, setAllGames] = useState([])
  const [showLoader, setLoader] = useState(false)

  const getAllGames = async () => {
    setLoader(true)

    await axios.get(`${host}/api/v1/games/list`, { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
      .then(response => {
        setLoader(false)
        setAllGames(response.data)
      }).catch(error => {
        setLoader(false)
        console.log('Something went wrong')
      })
  }

  useEffect(() => {
    getAllGames()
  }, [])

  return (
        <>
            <Container >
                <div className="row cards-wrapper">
                {showLoader
                  ? (<div className="d-flex w-100 flex-column align-items-center justify-content-center loader-container">
                    <ThreeDots hheight="60"
                        width="60"
                        radius="9"
                        color="red"
                        ariaLabel="three-dots-loading"
                        visible={true}/>
                    <div className="h4 text-muted">Loading...</div>
                    </div>
                    )
                  : (<GamesCard games={allGames}></GamesCard>)}
                </div>
            </Container>
        </>
  )
}
