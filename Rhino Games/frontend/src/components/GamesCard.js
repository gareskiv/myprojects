import { Card, Button, Modal } from 'react-bootstrap'
import { useSelector } from 'react-redux'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { slice } from 'lodash'

function GameCard ({ games }) {
  const [modalId, setModalId] = useState('')
  const handleOpenModal = (id) => {
    setModalId(id)
  }
  const handleCloseModal = () => {
    setModalId(null)
  }

  const isLogged = useSelector(state => state.loggedUser.data.isLogged)
  const navigate = useNavigate()
  const goToLogin = (id) => {
    handleCloseModal()
    return navigate('/login', { state: { id } })
  }

  const searchBarInput = useSelector(state => state.updateSearchInput)
  const filteredData = games.filter((el) => {
    if (searchBarInput.searchInput === '') {
      return el
    } else {
      return el.title.toLowerCase().includes(searchBarInput.searchInput) || el.manufacturer.toLowerCase().includes(searchBarInput.searchInput)
    }
  })

  const [isCompleted, setIsCompleted] = useState(false)
  const [index, setIndex] = useState(8)
  const initialGames = slice(filteredData, 0, index)
  const loadMore = () => {
    setIndex(index + 4)

    if (games[index + 5]) {
      setIsCompleted(false)
    } else {
      setIsCompleted(true)
    }
  }

  const playCurrentGame = (id) => {
    navigate(`/play-game/${id}`)
  }

  return (
        <>
            {initialGames.length > 0
              ? (
            <>
                {initialGames.map((data) => (
                    <div className="col-lg-3 col-sm-4" key={data.id}>
                        <Card className="game-card my-2" id={`game-${data.id}`}>
                            <Card.Img variant="top" src={`./images/${data.cover}`} alt="card-image" className="card-image"/>
                            <Card.Body className="custom-card-body">
                                    <Card.Title className="text-center">{data.title}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted text-center">{data.manufacturer}</Card.Subtitle>
                                <div className="form-btn-container mb-2">
                                    {!isLogged
                                      ? (<Button id={`game-button-${data.id}`} variant="danger" className="form-btn" onClick={() => handleOpenModal(data.id)} >Play for fun</Button>)
                                      : (
                                        <Button id={`game-button-${data.id}`} className="form-btn" variant="dark" onClick={() => playCurrentGame(data.id)}>Play for real</Button>)}
                                </div>
                            </Card.Body>
                        </Card>

                        <Modal className="form-card" id={`modal- ${data.id}`} show={modalId === data.id} onHide={handleCloseModal} centered={true}>
                            <Modal.Body>
                                <h5>
                                In order to play <b className="text-danger">{data.title}</b>  game, you must login first.
                                </h5>
                            </Modal.Body>
                            <Modal.Footer style={{ borderTop: 'none', display: 'flex', width: '100%', justifyContent: 'center' }}>
                                <Button className="border-btn" variant="danger" onClick={handleCloseModal}>
                                    Close
                                </Button>
                                <Button className="border-btn" variant="dark" onClick={() => goToLogin(data.id)}>
                                    Log In
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </div>
                ))}
                <div className="d-grid mt-3 mb-5">
                    {isCompleted || searchBarInput.searchInput != ''
                      ? (
                          ''
                        )
                      : (
                        <Button onClick={loadMore} variant="dark" className="border-btn">
                            Load More +
                        </Button>
                        )}
                </div>
            </>)
              : (<div className="col-lg-12 text-center">No data found.</div>)}
        </>
  )
}

export default GameCard
