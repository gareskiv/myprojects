
import { Route, Routes, Navigate } from 'react-router-dom'
import NotFoundComponent from './NotFoundComponent'
import ListGames from './ListGames'
import Settings from './Settings'
import SignUp from './SignUp'
import Login from './Login'
import GameForReal from './GameForReal'
import Protected from './security/ProtectedRoute'
import { useSelector } from 'react-redux'

export default function Index () {
  const isLogged = useSelector(state => state.loggedUser.data.isLogged)

  return <Routes>
      <Route path='*' element={<Navigate to="/404" replace={true} />} />
      <Route path='/404' element={<NotFoundComponent />} />
      <Route path="/" element={<ListGames/>}></Route>
      <Route path="/signup" element={<SignUp/>}></Route>
      <Route path="/login" element={<Login/>}></Route>
      <Route path="/settings" element={
        <Protected isAuthenticated={isLogged}>
          <Settings />
        </Protected>}>
      </Route>
      <Route path="/play-game/:id" element={
        <Protected isAuthenticated={isLogged}>
          <GameForReal/>
        </Protected>}>
      </Route>
  </Routes>
}
