import SignUp from './SignUp'
import { Button, Form, Card, Alert } from 'react-bootstrap'
import { Link, useNavigate, useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { loggedUserData } from '../actions'
import { useState } from 'react'
import { ThreeDots } from 'react-loader-spinner'
import axios from 'axios'

export default function Login () {
  let navigateLogin = false
  const host = 'http://localhost:8080'
  const dispatch = useDispatch()
  const location = useLocation()
  const navigate = useNavigate()
  const [showLoader, setLoader] = useState(false)
  const [formData, setFormData] = useState({
    username: '',
    password: ''
  })
  const [error, setError] = useState('')

  const checkFormData = e => {
    e.preventDefault()

    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  const userLogin = async (e) => {
    e.preventDefault()

    if (formData.username != '' && formData.password != '') {
      setError('')
      setLoader(true)

      await axios.post(`${host}/api/v1/users/auth/login`, formData, { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
        .then(response => {
          if (response.status === 200) {
            response.data.isLogged = true
          }
          dispatch(loggedUserData(response.data))
          navigateLogin = true
        }).catch(error => {
          setLoader(false)
          if (String(error.response.status)[0] === '4' && error.response.data.message) {
            setError(error.response.data.message)
          } else {
            setError('Something bad happened.')
          }
        }).finally(() => {
          if (navigateLogin && location.state != null && location.state.id) {
            return navigate(`/play-game/${location.state.id}`)
          }
        })
    }
  }

  return <div className="form-container">
        <Card className="py-4 form-card card-class">
            <div className="text-center">
                <h2>Login</h2>
                <h6 className="text-muted">
                 {!showLoader ? (<span>Please enter your username and password!</span>) : (<span>Logging in...</span>)}
                </h6>
            </div>
            <Card.Body>
                {error != '' ? (<Alert key="danger" variant="danger">{error}</Alert>) : ''}

                {showLoader
                  ? (
                    <div className="d-flex w-100 align-items-center justify-content-center">
                        <ThreeDots hheight="45"
                            width="45"
                            radius="9"
                            color="red"
                            ariaLabel="three-dots-loading"
                            visible={true}/>
                    </div>
                    )
                  : (
                    <Form onSubmit={userLogin}>
                        <Form.Group className="mb-3" controlId="username">
                            <Form.Label>Username</Form.Label>
                            <Form.Control value={formData.username} name="username" type="text" placeholder="Enter username" onChange={checkFormData} />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control value={formData.password} type="password" placeholder="Password" name="password" onChange={checkFormData}/>
                        </Form.Group>
                        <div className="form-btn-container w-100">
                            <Button variant="dark" type="submit" className="form-btn">
                                Log In
                            </Button>
                        </div>
                        <div className="text-center mt-3">
                            Don't have an account?
                            <Link className="px-1 text-danger" to="/signup" element={<SignUp/>}>Sign Up</Link>
                        </div>
                    </Form>)}
            </Card.Body>
        </Card>
    </div>
}
