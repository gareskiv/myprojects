export default function NotFoundComponent () {
  return <h1 className="text-center">404 NOT FOUND!</h1>
}
