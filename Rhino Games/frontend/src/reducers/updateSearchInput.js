const initialState = {
  searchInput: ''
}

const updateSearchInput = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE_SEARCH_INPUT':
      return {
        ...state,
        searchInput: action.payload
      }
    case 'CLEAR_SEARCH_INPUT':
      return initialState
    default:
      return state
  }
}

export default updateSearchInput
