const initialState = {
  activeLink: ''
}

const setActiveLink = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_ACTIVE_LINK':
      return {
        ...state,
        activeLink: action.link
      }
    case 'CLEAR_ACTIVE_LINK':
      return initialState
    default:
      return state
  }
}

export default setActiveLink
