const initialState = {
  data: {
    id: '',
    username: '',
    email: '',
    token: '',
    isLogged: false
  }
}

const loggedUserData = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGGED_USER_DATA':
      return {
        ...state,
        data: action.payload
      }
    case 'CLEAR_LOGGED_USER_DATA':
      return initialState
    default:
      return state
  }
}

export default loggedUserData
