import { combineReducers } from 'redux'
import updateSearchInput from './updateSearchInput'
import loggedUserData from './loggedUserData'
import setActiveLink from './setActiveLink'

const allReducers = combineReducers({
  updateSearchInput,
  loggedUser: loggedUserData,
  setActiveLink
})

export default allReducers
