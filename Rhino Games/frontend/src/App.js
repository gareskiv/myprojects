import 'bootstrap/dist/css/bootstrap.min.css'
import './style/index.scss'
import Container from 'react-bootstrap/Container'
import Header from './components/layout/Header'
import Footer from './components/layout/Footer'
import RoutesComponent from './components/Router.js'
import { useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import { setActiveLink } from './actions'
import { useDispatch, useSelector } from 'react-redux'

function App () {
  const dispatch = useDispatch()
  const isLogged = useSelector(state => state.loggedUser.data.isLogged)
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    if (location.pathname == '/login' && isLogged) {
      navigate('/')
    }
    dispatch(setActiveLink(location.pathname))
  })

  return (
    <div className="App">
      <Header/>
      <Container className="main-container">
        <RoutesComponent/>
      </Container>
      <Footer/>
    </div>
  )
}

export default App
