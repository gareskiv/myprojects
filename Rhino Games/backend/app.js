const path = require('path');
const express = require('express');
const cors = require('cors');
const socketIO = require('socket.io');
const cookieSession = require('cookie-session');
const { dbConfig } = require('./db/config');
const { initRoutes } = require('./src/routes');
const { socketConnect, socketGame } = require('./src/socket.io/mainSocket');
const { errorHandler } = require('./src/middlewares/error');

require('dotenv').config({ path: process.env.NODE_ENV === 'development' ? path.join(__dirname, 'config', '.env') : path.join(__dirname, 'config', `.env.${process.env.NODE_ENV}`) });
const PORT = process.env.PORT;

const app = express();

app.use(cors({
  origin: `http://localhost:3001`,
  methods: ['POST', 'PUT', 'GET', 'OPTIONS', 'HEAD'],
  credentials: true
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(express.static('src/static/'));

app.use(
    cookieSession({
      name: 'user-session',
      secret: process.env.COOKIE_SECRET,
      httpOnly: true,
      maxAge: parseInt(process.env.COOKIE_LIFE)
    })
);

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept');
    next();
});  

dbConfig();
initRoutes(app);

app.use(errorHandler);

const server = app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});

const io = socketIO(server, {
    cors: {
      origin: '*'
    }
});

socketConnect(io);
socketGame(io);
