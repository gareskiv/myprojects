const path = require('path');
const { knexSnakeCaseMappers } = require('objection');

require('dotenv').config({ path: process.env.NODE_ENV === 'development' ? path.join(__dirname, 'config', '.env') : path.join(__dirname, 'config', `.env.${process.env.NODE_ENV}`) });
const { DB_USER, DB_NAME, DB_PASSWORD, DB_HOST, DB_PORT } = process.env;

const knexConfig = {
  development: {
    client: 'postgresql',
    connection: {
      database: DB_NAME,
      user: DB_USER,
      password: DB_PASSWORD,
      host: DB_HOST,
      port: DB_PORT
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds',
    },
    ...knexSnakeCaseMappers
  }
};

module.exports = knexConfig;