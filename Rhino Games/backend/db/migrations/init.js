const up = (knex) => {
    return knex.schema
     .createTable('users', (table) => {
       table.increments('id').primary();
       table.string('email').notNullable().unique();
       table.string('username').notNullable().unique();
       table.string('password').notNullable();
       table.timestamps(true, true);
     })
     .createTable('games', (table) => {
       table.increments('id').primary();
       table.string('title').notNullable().unique();
       table.string('cover').notNullable();
       table.string('manufacturer').notNullable();
       table.text('description');
       table.timestamps(true, true);
     });
 };
 
 const down = (knex) => {
   return knex.schema
     .dropTableIfExists('users')
     .dropTableIfExists('games');
 };
 
 module.exports = {
   up,
   down
 };