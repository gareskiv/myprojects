const bcrypt = require('bcrypt');

const seed = async (knex) => {
    await knex.raw('TRUNCATE TABLE "users" CASCADE');
    await knex.raw('TRUNCATE TABLE "games" CASCADE');
  
    await knex('users').insert([
      { id: 1, email: 'admin@example.com', username: 'admin', password: bcrypt.hashSync('admin123', 10) },
      { id: 2, email: 'test@example.com', username: 'test', password: bcrypt.hashSync('test123', 10) }
    ]);
  
    await knex('games').insert([
        {   
            id: 1,
            title: "Super Mario Bros",
            cover: "1.jpg",
            manufacturer: "Nintendo",
            description: "Super Mario Bros. is a classic platform game featuring Mario, Luigi, and other characters from the popular Mario franchise. Players must guide Mario through various levels, collecting coins and power-ups while avoiding obstacles and enemies."
        },
        {   
            id: 2,
            title: "The Legend of Zelda",
            cover: "2.jpg",
            manufacturer: "Nintendo",
            description: "The Legend of Zelda is an action-adventure game set in a vast and immersive fantasy world. Players control Link, a young hero tasked with saving the kingdom of Hyrule from the evil Ganon. Along the way, they must solve puzzles, defeat enemies, and collect powerful items."
        },
        {   
            id: 3,
            title: "Pac-Man",
            cover: "3.jpg",
            manufacturer: "Namco",
            description: "Pac-Man is a classic arcade game in which players guide the eponymous character through a maze, eating dots and avoiding ghosts. The game was wildly popular in the 1980s and has since been released on a wide variety of platforms."
        },
        {   
            id: 4,
            title: "Tetris",
            cover: "4.jpg",
            manufacturer: "Nintendo",
            description: "Tetris is a puzzle game in which players must fit falling tetrominoes into a well, creating complete rows to clear them. The game has been released on a wide variety of platforms and is one of the best-selling video games of all time."
        },
        {   
            id: 5,
            title: "Donkey Kong",
            cover: "5.jpg",
            manufacturer: "Nintendo",
            description: "Donkey Kong is a classic arcade game in which players control Jumpman (later known as Mario) as he tries to rescue Pauline from the eponymous gorilla. The game was the first to feature Mario and has been released on numerous platforms."
        },
        {   
            id: 6,
            title: "Pong",
            cover: "6.jpg",
            manufacturer: "Atari",
            description: "Pong is a classic arcade game in which players control paddles on opposite sides of the screen and try to hit a ball back and forth. The game was one of the first to gain widespread popularity and has been released on a variety of platforms."
        },
        {   
            id: 7,
            title: "Space Invaders",
            cover: "7.avif",
            manufacturer: "Taito",
            description: "Space Invaders is a classic arcade game in which players control a spaceship and try to defeat waves of alien invaders. The game was hugely popular in the 1970s and has been released on numerous platforms."
        },
        {   
            id: 8,
            title: "Galaga",
            cover: "8.jpeg",
            manufacturer: "Namco",
            description: "Galaga is a classic arcade game in which players control a spaceship and try to defeat waves of alien attackers. The game is known for its challenging gameplay and has been released on numerous platforms."
        },
        {   
            id: 9,
            title: "Centipede",
            cover: "9.avif",
            manufacturer: "Atari",
            description: "Centipede is a classic arcade game in which players control a spaceship and try to defeat a centipede that is moving down the screen. The game is known for its colorful graphics and has been released on numerous platforms."
        },
        {   
            id:10,
            title: "Missile Command",
            cover: "10.jpg",
            manufacturer: "Atari",
            description: "Missile Command is a classic arcade game in which players control a series of missile launchers and try to defend their cities from incoming missiles. The game is known for its fast-paced gameplay and has been released on numerous platforms."
        },
        {   
            id: 11,
            title: "Paperboy",
            cover: "11.jpeg",
            manufacturer: "Atari",
            description: "Paperboy is a classic arcade game in which players control a paperboy and try to deliver newspapers to houses while avoiding obstacles. The game is known for its unique premise and has been released on numerous platforms."
        },
        {   
            id: 12,
            title: "Robotron: 2084",
            cover: "12.jpg",
            manufacturer: "Williams Electronics",
            description: "Robotron: 2084 is a classic arcade game in which players control a character and try to defeat waves of enemies in a futuristic world. The game is known for its fast-paced gameplay and has been released on numerous platforms."
        },
        {   
            id: 13,
            title: "Defender",
            cover: "13.jpeg",
            manufacturer: "Williams Electronics",
            description: "Defender is a classic arcade game in which players control a spaceship and try to defend against waves of enemies and rescue humanoids. The game is known for its challenging gameplay and has been released on numerous platforms."
        },
        {   
            id: 14,
            title: "Joust",
            cover: "14.jpg",
            manufacturer: "Williams Electronics",
            description: "Joust is a classic arcade game in which players control a knight riding a flying ostrich and try to defeat enemy knights. The game is known for its unique premise and has been released on numerous platforms."
        }
    ]);
  };
  
  module.exports = {
    seed
  };