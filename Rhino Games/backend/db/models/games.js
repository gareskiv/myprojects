const { Model } = require('objection');

class Games extends Model {
  static get tableName() {
    return 'games';
  }
}

module.exports = Games;