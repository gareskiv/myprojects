-- for help: \?

-- DATABASE --
-- list : \l
-- create : CREATE DATABASE database_name;
-- switch : \c database_name

-- TABLES --
-- list all : \d
-- list specific : \d table_name

CREATE TABLE users (
    id INT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
    email VARCHAR(50) NOT NULL UNIQUE,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL
);

CREATE TABLE games (
    id INT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    cover VARCHAR(50) NOT NULL,
    manufacturer VARCHAR(50) NOT NULL,
    description TEXT
);