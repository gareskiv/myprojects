const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../../db/models/users');

const userSignup = async (req, res) => {
  try {
    const user = await User.query().findOne({ username: req.body.username });
    
    if (user) {
        return res.status(400).send({ message: 'User already exists' });
    }

    const newUser = await User.query().insertAndFetch({
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10)
    });
  
    if (!newUser) {
        return res.status(400).send({ message: 'Registration failed' });
    }
  
    res.status(200).send({ message: 'Successfully registered' });
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

const userLogin = async (req, res) => {
  try {
    const user = await User.query().findOne({ username: req.body.username });

    if (!user) {
        return res.status(404).send({ message: 'User not found' });
    }
    
    const validPassword = bcrypt.compareSync(req.body.password, user.password);

    if (!validPassword) {
        return res.status(401).send({ message: 'Invalid password' });
    }

    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, { expiresIn: parseInt(process.env.JWT_LIFE) });
    req.session.token = token;
    
    res.status(200).send({
        id: user.id,
        username: user.username,
        email: user.email,
        token
    });
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

const userLogout = async (req, res) => {
  try {
    req.session = null;
    res.status(200).send({ message: 'Signed out' });
  } catch (error) {
    this.next(error);
  }
};

module.exports = {
  userSignup,
  userLogin,
  userLogout
};