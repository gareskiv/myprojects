const Game = require('../../db/models/games');

const gameList = async (req, res) => {
    try {
        const games = await Game.query().select('*');
        res.status(200).send(games || []);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};
  
const gamePlay = async (req, res) => {
    try {
        const game = await Game.query().findById(req.params.id);

        if (!game) {
            return res.status(404).send({ message: 'Game not found' });
        }
      
        res.status(200).send(game);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};
  
module.exports = {
    gameList,
    gamePlay
}
