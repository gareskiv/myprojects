const User = require('../../db/models/users');

const settingsGet = async (req, res) => {
  try {
    const user = await User.query().findById(req.params.id);


    if (!user) {
        req.session = null;
        return res.status(404).send({ message: 'User not found' });
    }
  
    rres.status(200).send({
        id: user.id,
        username: user.username,
        email: user.email
    });  
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

const settingsUpdate = async (req, res) => {
   try {
    const id = req.params.id;
    const userUpdate = await User.query().patchAndFetchById(id, {
        username: req.body.username,
        // email: req.body.email
    });
  
    res.status(200).send({
        id,
        username: userUpdate.username,
        email: userUpdate.email,
    });  
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

module.exports = {
  settingsGet,
  settingsUpdate
};