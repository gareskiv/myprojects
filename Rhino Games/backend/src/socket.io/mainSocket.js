let xPosition = 50;
let yPosition = 50;

const socketConnect = (io) => {
  io.on('connection', (socket) => {
    socket.broadcast.emit('join', 'New player has joined');
    socket.emit('init', { xPosition, yPosition });
    
    socket.on('start', name => {
      console.log(`${name} says hello`);
      socket.emit('name', name);
    });

    socket.on('disconnect', () => {
      console.log(`${socket.id} disconnected`);
      socket.broadcast.emit('leave', 'Player left');
    });
  });
};

const socketGame = (io) => {
  io.on('connection', socket => {
    socket.on('chat', data => {
      console.log(`${data.user} says: ${data.message}`);
      io.sockets.emit('chat', { user: data.user, message: data.message });
    });

    socket.on('movement', data => {
      console.log(data);
      xPosition = data.xPosition;
      yPosition = data.yPosition;
      io.sockets.emit('movement', data);
    });
  });
};

module.exports = {
  socketConnect,
  socketGame
};