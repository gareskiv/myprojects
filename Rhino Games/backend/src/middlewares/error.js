const errorHandler = (err, req, res, next) => {
    console.log('Final stop error handler');
  
    const status = err.statusCode || 500;
    const message = err.message || 'Something went wrong';
    
    res.status(status).send({
        success: false,
        status,
        message,
        stack: process.env.NODE_ENV === 'development' ? err.stack : {}
    });
  }
  
  module.exports = {
    errorHandler
  }