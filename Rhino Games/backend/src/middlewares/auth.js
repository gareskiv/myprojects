const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
  const token = req?.session?.token;

  if (!token) {
    return res.status(403).send({ message: 'Missing token' });
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: 'Unauthorized' });
    }
    req.userId = decoded.id;
    next();
  });
};

module.exports = {
  verifyToken
}
