const router = require('express').Router();
const { body } = require('express-validator');
const { userSignup, userLogin, userLogout } = require('../controllers/users.controller');
const { validateInput } = require('../middlewares/validator');

router.post('/auth/signup', [
    body('email')
    .exists().withMessage('Email is required')
    .isEmail().withMessage('Invalid email format'),
    body('username')
    .exists().withMessage('Username is required')
    .isAlphanumeric().withMessage('Username should be alphanumeric')
    .isLength({ min: 5 }).withMessage('Username too short'),
    body('password')
    .exists().withMessage('Password is required')
    .isStrongPassword({ 
      minLength: 8,
      minLowercase: 1,
      minUppercase: 1,
      minNumbers: 1,
      minSymbols: 1,
      returnScore: false,
      pointsPerUnique: 1,
      pointsPerRepeat: 0.5,
      pointsForContainingLower: 10,
      pointsForContainingUpper: 10,
      pointsForContainingNumber: 10,
      pointsForContainingSymbol: 10,
     }),
     validateInput
], userSignup);

router.post('/auth/login', [
    body('username')
    .exists().withMessage('Username is required'),
    body('password')
    .exists().withMessage('Password is required'),
    validateInput
], userLogin);

router.get('/auth/logout', userLogout);
  
module.exports = router;
  