const socketio = require('./socketio.route');
const users = require('./users.route');
const settings = require('./settings.route');
const games = require('./games.route');

const initRoutes = (app) => {
    app.use('/socketio', socketio);
    app.use('/api/v1/users', users);
    app.use('/api/v1/settings', settings);
    app.use('/api/v1/games', games);
};
  
module.exports = {
    initRoutes
}
  