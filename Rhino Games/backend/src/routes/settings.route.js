const router = require('express').Router();
const { body, param } = require('express-validator');
const { settingsGet, settingsUpdate } = require('../controllers/settings.controller');
const { verifyToken } = require('../middlewares/auth');
const { validateInput } = require('../middlewares/validator');

router.get('/:id', [
    verifyToken,
    param('id')
    .exists().withMessage('ID parameter is required')
    .isInt({ min: 1 }).withMessage('ID must be an integer greated than 0'),
    validateInput
], settingsGet);

router.post('/:id', [
    verifyToken,
    param('id')
    .exists().withMessage('ID parameter is required')
    .isInt({ min: 1 }).withMessage('ID must be an integer greated than 0'),
    // body('email')
    // .exists().withMessage('Email is required')
    // .isEmail().withMessage('Invalid email format'),
    body('username')
    .exists().withMessage('Username is required')
    .isAlphanumeric().withMessage('Username should be alphanumeric')
    .isLength({ min: 5 }).withMessage('Username too short'),
    validateInput
], settingsUpdate);
  
module.exports = router;
  