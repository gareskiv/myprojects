const router = require('express').Router();
const { param } = require('express-validator');
const { gameList, gamePlay } = require('../controllers/games.controller');
const { verifyToken } = require('../middlewares/auth');
const { validateInput } = require('../middlewares/validator');

router.get('/list', gameList);

router.get('/play/:id', [
    verifyToken,
    param('id')
    .exists().withMessage('ID parameter is required')
    .isInt({ min: 1 }).withMessage('ID must be an integer greated than 0'),
    validateInput
], gamePlay);
  
module.exports = router;
  