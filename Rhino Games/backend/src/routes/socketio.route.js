const path = require('path');
const router = require('express').Router();
const { verifyToken } = require('../middlewares/auth');

const staticPath = path.join(__dirname, '..', 'static');

router.get('/', [verifyToken], (req, res) => {
  res.status(200).sendFile(staticPath + '/index.html')
});

module.exports = router;