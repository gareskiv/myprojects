# React-Games

Welcome to React-Games! This application is designed to be run using Docker and Docker Compose, so make sure to have them installed and running on your machine before proceeding.

Please also ensure that the ports listed below are available on your local machine and not in use by any other processes, as this may prevent the application from starting properly:

- [3001] for the frontend
- [8080] for the backend
- [5432] for the Postgresql service

## Running the application

To start the application, follow these steps:

1. Clone the repository to a destination on your machine using `git clone https://gitlab.com/gareskiv/rhino-demo-games`.
2. Open a terminal and navigate to the repository directory using `cd <repo-name>`.
3. Navigate to the `docker` directory within the repository using `cd docker`.
4. Run `NODE_ENV=development docker-compose build`.
5. Run `NODE_ENV=development docker-compose up`.

Once the application is running, you can access the frontend at `localhost:3001 `in your web browser. From there, you can create a new account, log in, and play some games.

**Note: the `NODE_ENV` variable is used with the `docker-compose `command as part of the environment configuration.**

I hope you enjoy using React-Games!
