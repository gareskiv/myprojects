@extends('errors::minimal')

@section('title', __($exception->getMessage()))
@section('code', '400')
@section('message', __($exception->getMessage()))
