
<!DOCTYPE html>
<html>
<head>
	<title>Message</title>		
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">	
</head>
<body>
  
  <p>Verification code: <strong>{{$code}}</strong></p>

  <a href="{{$link}}">Activate Your Account!</a>

</body>
</html>