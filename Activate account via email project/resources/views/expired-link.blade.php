@extends('layouts.app')
@section('script')
    <style>
        nav{
            display:none !important;
        }

        .main-container {
            min-height:77.3vh;
            display:flex;
            flex-direction:column;
            justify-content:center;
            align-items:center;            
        }
        .text-div {
            margin-bottom:40px;
        }
    </style>
@endsection
@section('content')
        <div class="container">
            <div class="row">   
          @if(Session('status')) 
            Success
          @endif
                <div class="main-container col-md-12">
                    <h2 class="text-div">Expired verification code.</h2>                
                    <form method="POST" action="{{ route('reinvite') }}"> 
                        @csrf
                        <input type="hidden" name="email" value="{{$user->email}}">
                        <button type="submit" class="btn btn-primary" href="#">Resend!</button>
                    </form>
                </div>
            </div>
        </div>
@endsection