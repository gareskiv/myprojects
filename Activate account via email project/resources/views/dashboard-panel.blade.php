@extends('layouts.app')

@section('content') 
<div class="container">
    <div class="row">     
        <div class="col-md-12" style="margin-bottom:20px;">
            <a href="{{ route('register') }}" class="btn btn-primary">Register User</a>
        </div>   
        <div class="table-container col-md-12">        
            <table class="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">Username</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Status</th>        
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $user)   
                    <tr>
                        <td>@if( !$user->username )
                                 <span class="text-muted"> available after activation </span>
                                 @else {{$user->username}} 
                            @endif
                        </td>
                        <td>{{$user->email}}</td>
                        <td>@if($user->active) 
                            <span class="text-success">Active</span> 
                            @else <span class="text-danger">Not Active</span> 
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection