<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        \DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@example.com',
            'password' => \Hash::make('admin'),
            'is_admin' => 1,           
            'active' => 1,
        ]);
    }
}
