<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('/');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'UserController@index')->name('user');
Route::get('/activate', 'UserController@activate')->name('activate');

Route::get('/dashboard/panel', 'AdminController@index')->name('admin')->middleware('role');
Route::get('/dashboard/register', 'AdminController@register')->name('register')->middleware('role');

Route::post('/dashboard/register', 'AdminController@invite')->name('invite')->middleware('role');
Route::post('/activate', 'UserController@activateProfile')->name('account');

Route::post('/verification/expired/resend', 'UserController@reinvite')->name('reinvite');
