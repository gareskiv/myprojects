<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVerificationLink
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)    
    {   
        $data['username'] = $event->user->username;        
        $data['email'] = $event->user->email;
        $data['code'] = $event->user->code;
        $data['link'] = env('APP_URL').'activate'.'?email='.$event->user->email.'&code='.$event->user->verification_key;

        Mail::send('email.verify', $data, function($em) use ($data){
            $em->from('no-replay@example.com');
            $em->to($data['email'])->subject('Invitation!');

        });
    }
}
