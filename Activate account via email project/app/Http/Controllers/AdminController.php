<?php

namespace App\Http\Controllers;
use App\User;

use App\Http\Requests\InviteRequest;
use App\Events\RegisterUser;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() 
    {   
        $user = User::all();

        return view('dashboard-panel', ['user' => $user]);
    }

    public function register() 
    {   
       return view('register-user');
    }

    public function invite(InviteRequest $request) 
    {   
      $user = new User();      
      $user->email = $request->get('email');
      $user->code = rand(100000, 999999);
      $user->is_admin = 0;
      $user->verification_expires = date('Y-m-d H:i:s', strtotime('+5 minutes'));
      $user->verification_key= \Str::random(80);

      if($user->save()) {
          event(new RegisterUser($user));
          
          \Session::flash('success');
          return back();
        }
    }
  
       
}
