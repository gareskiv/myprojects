<?php

namespace App\Http\Controllers;

use App\User;

use App\Events\ActivateUser;
use App\Events\RegisterUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() 
    {   
        return view('home');
    }

    public function activate(Request $request) 
    {
        $input = $request->all();
        
       
        $user = User::where('email', $input['email'])->where('verification_key', $input['code'])->first();
       
        
        if(!$user) {   
            abort(400, 'Bad request.');
        }

        if(strtotime($user->verification_expires) < time()) {
            return view('expired-link', ['user' => $user]);
        }

        return view('activate-profile', ['user' => $user]);
    }

    public function activateProfile(Request $request) 
    {
        $input = $request->all();

        $validatedData = $request->validate([
            'email' => 'email',
            'code' => ['required','numeric','min:6'],            
            'username' =>['required','string','max:255', 'unique:users'],
            'password' => ['required', 'max:255'],
        ]);
            
        $user = User::where('email', $input['email'])->where('code', $input['code'])->first();
            
        if(!$user) {
            abort(401);
        }        

        try {
            $user->password = \Hash::make($input['password']);
            $user->username = $input['username'];
            $user->code = null;
            $user->verification_key = null;

            event(new ActivateUser($user));      
            $user->save();
            return redirect('login');
        } catch (\Throwable $e) {
            report($e);
            abort(400, 'Something bad happened.');
        }       
    } 

    

    public function reinvite(Request $request) {
        $input = $request->all();
        
        $user = User::where('email', $input['email'])->first();        
       
        $user->code = rand(100000, 999999);       
        $user->verification_expires = date('Y-m-d H:i:s', strtotime('+5 minutes'));
        $user->verification_key= \Str::random(80);
        if($user->update()) {
            event(new RegisterUser($user));
            
            \Session::flash('resend');

            return redirect()->route('/');
           
        }

    }
}
