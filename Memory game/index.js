$(document).ready(function(){

    let app = {
        cards: ['red','red','yellow','yellow','green','green','blue','blue','orange','orange','purple','purple'],
             

        init: function() {
            app.shuffle();           

        },

        shuffle: function() {
            let random = 0;
            let temp = 0;
            for(let i = 1; i < app.cards.length; i++) {
                random = Math.round(Math.random() * i);
                temp = app.cards[i];
                app.cards[i] = app.cards[random];
                app.cards[random] = temp;
            }
            app.assignCards();

            console.log("shuffeled card array:", app.cards);
        },

        assignCards: function() {
            $('.card').each(function(index) {
                $(this).attr('data-card-value', app.cards[index]);
            });
            app.clickHandlers();
        },

        clickHandlers: function() {
            $('.card').on('click', function(){               
                $(this).css('background-color', $(this).data('cardValue')).addClass('selected');
                app.checkMatch();
            });    
        },     

        checkMatch: function() {
            if ($('.selected').length == 2) {
                if ($('.selected').first().data('cardValue') == $('.selected').last().data('cardValue')) {
                    $('.selected').each(function() {
                        $(this).animate({opacity:0}).removeClass('unmatched');
                    });                   

                    $('.selected').each(function() {
                        $(this).removeClass('selected');
                        $(this).css('background-color', 'lightblue');      
                    }); 
                    
                    app.checkWin(); 
                } else {
                    setTimeout(function() {
                        $('.selected').each(function() {
                            $(this).html('').removeClass('selected');
                            $(this).css('background-color', 'lightblue');
                        });
                    }, 700);
                }                           
            }
        },

        checkWin: function() {
            if ($('.unmatched').length === 0) {
               alert('You Won!');         
               app.clickedBtn();                                    
            }               
        },

        clickedBtn: function() {
            $('#button').css('display', 'inline');
            $('#button').on('click', function() {
                location.reload();  
            //    $('.card').removeAttr('style').addClass('unmatched');            
            //    $('#button').css('display', 'none'); 
            });                    
            app.init();                     
        }
    };

    app.init();
});