<?php 

session_start();
// session_destroy();

require_once 'db.php';

if (!isset($_SESSION['visitor']) || !isset($_GET['id']) || intval($_GET['id']) <= 0) {
    header('Location: index.php');
    exit();
}

if (isset($_GET['id'])) {
    $content = "";

    $id = intval($_GET['id']);
    $pdoData['id'] = $id;
    
    $sql = "SELECT cards.id, cards.title, cards.cards_content, cards.materials, duration.duration_time, group_size.members, facility_level.level, category.category_name 
    FROM cards JOIN duration ON cards.duration_fk = duration.id JOIN group_size ON cards.group_size_fk = group_size.id JOIN facility_level ON cards.facility_fk = facility_level.id LEFT JOIN category ON cards.category_fk = category.id
    WHERE cards.id = :id 
    ORDER BY cards.id ASC";

    $stmt = $db_connection->prepare($sql);
    $stmt->execute($pdoData);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    if (isset($result['cards_content'])) {
        $content = file_get_contents('game-content/'.$result['cards_content']);
    }
}
?>

<!DOCTYPE html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/navbar-style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/single-page-style.css" rel="stylesheet">
    <title>Brainster</title>
</head>

<body>
    <header class="nav-header navbar-fixed-top nav-header-background-second">
        <nav>
            <ul class="nav-links">
                <li class="list-item"><a class="menu-btn" href=# type="button" class="btn btn-primary btn-lg"
                        data-toggle="modal" data-target="#modal"><i class="fas fa-bars"></i> Мени</a></li>
            </ul>
        </nav>
        <img id="logo" class="logo scroll-logo" src="images/logo.png" alt="logo">
        <div class="created-by-text-responsive white">Изработено од студентите на академијата за програмирање <a
                class="underline" href="https://brainster.co/" target="_blank">Brainster.</a></div>
        <div class="cta">
            <a href="https://brainster.co/business" target="_blank" class="button yellow-btn btn-md">Обуки за
                компании</a>
            <a href="register.php" target="_self" class="button black-btn btn-md">Вработи наши
                студенти</a>
        </div>
    </header>
    <div id="single-page-pic-container">  
    </div>   
    <div id="skew-white"></div>    
    <div class="container-fluid" id="top-container-single">
        <div class="row" id="top-row-single">            
            <div class="container" id="content-container">                
                <div class="row">
                    <div class="col-md-12 h1 game-title"><?= $result['title'] ?></div>                   
                </div>
                </br>
                <div class="row gray-section-game">                                     
                    <div class="col-md-3 col-sm-6 col-xs-12 section-font"><i class="far fa-clock fa-2x"></i> Time Frame                    
                        <div class="text-muted indent single-page-smalltext"><?= $result['duration_time'] ? $result['duration_time'] : '/' ?></div>
                    </div>                   
                    <div class="col-md-3 col-sm-6 col-xs-12 section-font"><i class="fas fa-user-friends fa-2x"></i> Group Size
                        <div class="text-muted indent single-page-smalltext"><?= $result['members'] ? $result['members'] : '/' ?></div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 section-font"><i class="fas fa-university fa-2x"></i> Facilitation Level
                        <div class="text-muted indent single-page-smalltext"><?= $result['level'] ? $result['level'] : '/' ?></div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 section-font"><i class="fas fa-paint-roller fa-2x"></i> Materials
                        <div class="text-muted indent single-page-smalltext"><?= $result['materials'] ? $result['materials'] : '/' ?></div>
                    </div>
                </div>
                <br>                
                <div class="main-game-text">
                   <?= $content ?>
                </div>
            </div>                 
        </div>       
        <div class="row">                          
            <div id="bottom-cut">            
                <div id="skew-purple-bottom"></div>                                
                <div class="container" id="bottom-title">
                    <div class="col-md-12 white text-center">
                        <h1 class="future-title">FUTURE-PROOF YOUR ORGANIZATION</h1>
                        <div class="row">
                            <div class="col-md-3 col-sm-1 col-xs-1"></div>
                            <div class="col-md-6 col-sm-10 col-xs-10">
                                <p class="paragraph-text">Find out how to unlock progress in your business. Understand
                                    your current state,
                                    identify opportunity areas
                                    and prepare to take charge of your organization's future.
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-1 col-xs-1"></div>
                        </div>
                    </div>
                        <div class="col-md-12 text-center">
                            <br>                        
                            <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"
                                class="btn yellow-btn btn-md button assesment-btn">Take the assessment</a>
                        </div>
                    </div>
                </div>                          
            </div>
            <div class="row">
                <footer>                   
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" id="footer-2">
                        <ul class="footer-list" id="first-footer-list">
                            <li><a class="white-list" href="about.php">About Us</a></li>
                            <li><a class="white-list" href="contact.php">Contact</a></li>
                            <li><a class="white-list" href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank">Gallery</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" id="footer-3">
                        <img id="logo-bottom" src="images/logo.png" class="bottom-logo scroll-logo">
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" id="footer-4">
                        <ul class="footer-list" id="second-footer-list">
                            <li><a href="https://www.facebook.com/brainster.co" target="_blank" class="footer-icon"><i class="fab fa-linkedin-in fa-2x"></i></a></li>
                            <li><a href="https://twitter.com/BrainsterCo" target="_blank" class="footer-icon"><i class="fab fa-twitter fa-2x"></i></a></li>
                            <li><a href="https://www.facebook.com/brainster.co" target="_blank" class="footer-icon"><i class="fab fa-facebook-f fa-2x"></i></a></li>
                        </ul>
                    </div>    
                </footer>  
            </div>                 
        </div>       
    </div>         
   
    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog my-modal" role="document">
            <div class="modal-content my-modal">
                <header class="side-header">
                    <img src="images/logo.png" class="side-img">
                    <button class="close-button" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"><img src="images/close.svg" class="close-img"> Затвори
                        </span></button>
                </header>
                <div class="modal-body side-list">
                    <div class="list-group">
                        <a href="register.php" target="_self" class="list-group-item yellow-line">Регистрирај се</a>
                        <a href="#" class="list-group-item yellow-line" target="_self">Најави се</a>                       
                        <a href="about.php" target="_self" class="list-group-item black-line">За нас</a>
                        <a href="https://www.facebook.com/pg/brainster.co/photos/" class="list-group-item black-line" target="_blank">Галерија</a>
                        <a href="contact.php" target="_self" class="list-group-item black-line">Контакт</a>
                    </div>
                </div>
            </div>
        </div>
    </div>           
   
    
    <script src="script/jquery-3.4.1.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>
    <script>  
        jQuery(document).ready(function(){
            jQuery('.scroll-logo').click(function () {
                jQuery('body,html').animate({
                    scrollTop: 0
                }, 400);
                return false;
            });
        });
        
        jQuery(document).ready(function() {
            // Transition effect for navbar 
            jQuery(window).scroll(function() {
                // checks if window is scrolled more than 500px, adds/removes an Id
                if(jQuery(this).scrollTop() > 480) { 
                    jQuery('.nav-header').attr('id', 'nav-header-background-main');
                } else {
                    jQuery('.nav-header').attr('id', 'nav-header-background-second');
                }
            });
        });
        
    </script>
</body>

</html>