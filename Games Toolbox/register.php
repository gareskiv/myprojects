<?php
    require_once 'db.php';
    require_once 'validation.php';
    

    $sql1 = "SELECT * from employees_number";
    $data1 = $db_connection->query($sql1);

    $sql2 = "SELECT * from  sector";
    $data2 = $db_connection->query($sql2);

    if (isset($_POST['register'])) {
        $messages = [];

        $first_name =  validateInput($_POST['first_name']);
        if (!checkLength($first_name)) {
            $messages['first_name'] = "This field is required.";
        }

        $last_name =  validateInput($_POST['last_name']);
        if (!checkLength($last_name)){
            $messages['last_name'] = "This field is required.";
        }

        $company =  validateInput($_POST['company']);
        if (!checkLength($company)) {
            $messages['company'] = "This field is required.";
        }

        $email =  validateInput($_POST['email']);
        if (!checkLength($email)) {
            $messages['email'] = "This field is required.";
        } else if (!validateAddress($email)) {
            $messages['email'] = "You have to insert valid email address.";
        }

        $phone =  validateInput($_POST['phone']);
        if (!checkLength($phone)) {
            $messages['phone'] = "This field is required.";
        } else if (!validatePhone($_POST['phone'])) {
            $messages['phone'] = "This field must contain only numbers.";
        }

        if (!isset($_POST['employees_number'])) {
            $messages['employees_number'] = "This field is required.";
        } else {
            $employees_number = $_POST['employees_number'];
        }

        if (!isset($_POST['sector'])) {
            $messages['sector'] = "This field is required.";
        } else {
            $sector = $_POST['sector'];
        }

        if (!isset($_POST['message']) || strlen(trim($_POST['message'])) === 0) {
            $messages['text-area'] = "This field is required.";
        } else {
            $textarea = $_POST['message'];
        }
    
        if (sizeof($messages) === 0) {
            $data = [
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $company,
                'email' => $email,
                'phone' => $phone,                
                'employees_number' => $employees_number,
                'sector' => $sector,
                'message' => $textarea
            ];

            try {
                $select = "SELECT * FROM users WHERE email_address = :email OR phone = :phone" ;
                $stmt = $db_connection->prepare($select);
                $stmt->execute(['email' => $email, 'phone' => $phone]);
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
         
                if (sizeof($result) > 0) {
                    $messages['fail'] = "User with specified phone number or email already exists.";
                } else {
                    $insertsql = "INSERT INTO users (name, surname, company, email_address, phone, sector_fk, employees_fk, message) VALUES (:first_name, :last_name, :company, :email, :phone, :sector, :employees_number, :message)";
                    $stmt1 = $db_connection->prepare($insertsql);
                    $stmt1->execute($data);
                    $messages['successful'] = "Succesfuly registered.";
                    unset($_POST);
                }
            } catch (PDOException $e) {
                $messages['fail'] = "Failed to register.";
            }
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Brainster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/register.css" rel="stylesheet">
</head>
<body>
    
    <div class="purple-bg">         
        <div class="form-container">            
          <div class="container">                 
                <div class="row"> 
                    <?php if (isset($messages['successful'])) { ?>
                        <div class="alert my-alert" role="alert"><?php echo $messages['successful'] ?> </div>
                    <?php } else if (isset($messages['fail'])) {?>
                        <div class="alert my-alert" role="alert"><?php echo $messages['fail'] ?> </div>
                    <?php } ?>                   
                    <div class="col-md-5 col-sm-5 col-xs-5 background-img">
                        <div class="dark-bg">
                            <div class="title h1">Register</div>                           
                            <img src="images/logo.png" class="form-logo">
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-7 inputs">                             
                        <form class="form-register" method="post" action="">
                            <div class="form-group">
                                <label for="first_name">Name :</label>
                                <input type="text" class="form-control input" id="first_name" placeholder="Name" name="first_name" value="<?php echo isset($_POST['first_name']) ? $_POST['first_name'] : "" ?>">
                                <span class="text-danger" id="name-span"><?php echo isset($messages['first_name']) ? $messages['first_name'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Surname :</label>
                                <input type="text" class="form-control input" id="last_name" placeholder="Surname" name="last_name" value="<?php echo isset($_POST['last_name']) ? $_POST['last_name'] : "" ?>">
                                <span class="text-danger" id="last-name-span"><?php echo isset($messages['last_name']) ? $messages['last_name'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="company">Company :</label>
                                <input type="tex2t" class="form-control input" id="company" placeholder="Company name" name="company" value="<?php echo isset($_POST['company']) ? $_POST['company'] : "" ?>">
                                <span class="text-danger" id="company-span"><?php echo isset($messages['company']) ? $messages['company'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="email">Email adress :</label>
                                <input type="email" class="form-control input" id="email" placeholder="Email" name="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : "" ?>">
                                <span class="text-danger" id="email-span"><?php echo isset($messages['email']) ? $messages['email'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone number :</label>
                                <input type="tel" class="form-control input" id="phone" placeholder="Phone" name="phone" value="<?php echo isset($_POST['phone']) ? $_POST['phone'] : "" ?>">
                                <span class="text-danger" id="phone-span"><?php echo isset($messages['phone']) ? $messages['phone'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="employees_number">Employees number :</label>                        
                                <select id="employees_number" class="form-control input" name="employees_number">
                                    <option disabled hidden selected>Please Choose...</option>
                                    <?php
                                        while ($row = $data1->fetch()) {
                                            if ($row['id'] === $employees_number) {
                                                echo "<option value='".$row['id']."' selected>".$row['employees']."</option>";
                                            } else {
                                                echo "<option value='".$row['id']."'>".$row['employees']."</option>";
                                            }
                                        }
                                    ?>                     
                                </select>
                                <span class="text-danger" id="employees-span"><?php echo isset($messages['employees_number']) ? $messages['employees_number'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="sector">Sector :</label>                        
                                <select id="sector" class="form-control input" name="sector"> 
                                    <option disabled hidden selected>Please Choose...</option>                          
                                    <?php
                                        while ($row = $data2->fetch()) {
                                            if ($row['id'] === $sector) {
                                                echo "<option value='".$row['id']."' selected>".$row['sectors']."</option>";
                                            } else {
                                                echo "<option value='".$row['id']."'>".$row['sectors']."</option>";
                                            }
                                        }
                                    ?>        
                                </select>
                                <span class="text-danger" id="sector-span"><?php echo isset($messages['sector']) ? $messages['sector'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="message">Message:</label>
                                <textarea class="form-control input" id="message" rows="5" placeholder="Your Message" name="message" value=""></textarea>
                                <span class="text-danger" id="message-span"><?php echo isset($messages['text-area']) ? $messages['text-area'] : "" ?></span>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn register-button" name="register">Register</button>
                            </div>
                        </form>
                    </div>                   
                </div>
                <div class="row">
                    <div class="col-md-12 text-center footer">
                        <p>За да се вратиш на почетната страница <a class="click" href="index.php">Кликни тука.</a></p>
                    </div>
                </div>              
            </div>
        </div>
    </div>

    <script src="script/jquery-3.4.1.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>
    <script>
        jQuery('#first_name').on('focus', function() {
            jQuery('#name-span').text('');
        });

        jQuery('#last_name').on('focus', function() {
            jQuery('#last-name-span').text('');
        });

        jQuery('#company').on('focus', function() {
            jQuery('#company-span').text('');
        });

        jQuery('#email').on('focus', function() {
            jQuery('#email-span').text('');
        });
     
        jQuery('#phone').on('focus', function() {
            jQuery('#phone-span').text('');
        });     

        jQuery('#employees_number').on('focus', function() {
            jQuery('#employees-span').text('');
        });
       
        jQuery('#sector').on('focus', function() {
            jQuery('#sector-span').text('');
        });     
        
        jQuery('#message').on('focus', function() {
            jQuery('#message-span').text('');
        });   
    </script> 
</body>
</html>