let categoryFlag = jQuery('.category-active') ? jQuery('.category-active').data('id') : null;
let durationFlag = jQuery('.duration-active') ? jQuery('.duration-active').data('id') : null;
let groupSizeFlag = jQuery('.group-size-btn') ? jQuery('.group-size-active').data('id') : null;

jQuery('.category-btn').click(function(event) {
    jQuery('.category-btn').removeClass('category-active');
    jQuery(this).addClass('category-active');
    let category = jQuery(this).data('id');
    jQuery('#category-field').val(category);
    if (categoryFlag && categoryFlag === category) {
        jQuery('.category-btn').removeClass('category-active');
        jQuery('#category-all').addClass('category-active');
        jQuery('#category-field').val(0);
        categoryFlag = null;
    } else {
        categoryFlag = category;
    }
});

jQuery('.duration-btn').click(function(event) {
    jQuery('.duration-btn').removeClass('duration-active');
    jQuery(this).addClass('duration-active');
    let duration = jQuery(this).data('id');
    jQuery('#duration-field').val(duration);
    if (durationFlag && durationFlag === duration) {
        jQuery('.duration-btn').removeClass('duration-active');
        jQuery('#duration-field').val(0);
        durationFlag = null;
    } else {
        durationFlag = duration;
    }
});

jQuery('.group-size-btn').click(function(event) {
    jQuery('.group-size-btn').removeClass('group-size-active');
    jQuery(this).addClass('group-size-active');
    let groupSize = jQuery(this).data('id');
    jQuery('#group-size-field').val(groupSize);
    if (groupSizeFlag && groupSizeFlag === groupSize) {
        jQuery('.group-size-btn').removeClass('group-size-active');
        jQuery('#group-size-field').val(0);
        groupSizeFlag = null;
    } else {
        groupSizeFlag = groupSize;
    }
});

moveFilter();
window.onresize = function() {
    moveFilter();
}

function moveFilter() {
    if (jQuery(window).width() <= 768) {
        jQuery(function move() {
            let collapseSection = jQuery('#filter-section');
            jQuery('#collapseButton').append(collapseSection);
            jQuery('#filter-section').show();
        });
    } else {
        jQuery(function move() {
            let collapseSection = jQuery('#filter-section');
            jQuery('#outer-filter-section').append(collapseSection);
            jQuery('#filter-section').show();
        });
    }
}

jQuery('.card-url').click(function(event) {
    let id = jQuery(this).data('id');
    jQuery('#card-id').val(id);
});

jQuery('#email-modal').on('hidden.bs.modal', function() {
    jQuery('#modal-input').val('');
});