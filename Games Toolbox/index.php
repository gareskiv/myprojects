<?php 

session_start();
// session_destroy();

require_once 'db.php';
require_once 'validation.php';

$cat_total = 0;
$category = 0;
$duration = 0;
$group_size = 0;
$messages = [];
$failed = false;

$sql2 = "SELECT final.id, final.category_name, final.total FROM 
(SELECT cat.id, cat.category_name, count(c.id) AS total FROM category cat LEFT JOIN cards c ON cat.id = c.category_fk GROUP BY cat.id  UNION ALL 
SELECT cat.id, cat.category_name, count(c.id) AS total FROM category cat RIGHT JOIN cards c ON cat.id = c.category_fk WHERE cat.category_name IS NULL GROUP BY cat.id) final";
$data2 = $db_connection->query($sql2)->fetchAll(PDO::FETCH_ASSOC);

$sql3 = "SELECT * FROM duration";
$data3 = $db_connection->query($sql3)->fetchAll(PDO::FETCH_ASSOC);

$sql4 = "SELECT * FROM group_size";
$data4 = $db_connection->query($sql4)->fetchAll(PDO::FETCH_ASSOC);

if (!isset($_POST['search'])) {
    $sql1 = "SELECT cards.id, cards.big_picture, cards.title, cards.small_picture, cards.group_size_fk, cards.duration_fk, cards.category_fk, duration.duration_time, group_size.members, category.category_name 
    FROM cards JOIN duration ON cards.duration_fk = duration.id JOIN group_size ON cards.group_size_fk = group_size.id LEFT JOIN category ON cards.category_fk = category.id
    ORDER BY cards.id ASC";
    $data1 = $db_connection->query($sql1)->fetchAll();  
} else {
    $sql1 = "SELECT cards.id, cards.big_picture, cards.title, cards.small_picture, cards.group_size_fk, cards.duration_fk, cards.category_fk, duration.duration_time, group_size.members, category.category_name 
    FROM cards JOIN duration ON cards.duration_fk = duration.id JOIN group_size ON cards.group_size_fk = group_size.id LEFT JOIN category ON cards.category_fk = category.id ";

    $cat = ($_POST['category']);
    $dur = ($_POST['duration']);
    $gs = ($_POST['group_size']);

    $pdoData = [];

    if ($cat) {
        $category = intval($cat);
        $sql1 .= "WHERE category.id = :category ";
        $pdoData['category'] = $category;
    }

    if ($dur) {
        $duration = intval($dur);
        $sql1 .= $cat ? "AND duration.id = :duration " : "WHERE duration.id = :duration ";
        $pdoData['duration'] = $duration;
    }

    if ($gs) {
        $group_size = intval($gs);
        $sql1 .= $cat || $dur ? "AND group_size.id = :group_size " : "WHERE group_size.id = :group_size ";
        $pdoData['group_size'] = $group_size;
    }

    $sql1 .= "ORDER BY cards.id ASC";
    $stmt = $db_connection->prepare($sql1);
    $stmt->execute($pdoData);
    $data1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

if (isset($_POST['modal_submit'])) {
    $id = intval($_POST['card_id']);
    $emailVisitor = $_POST['modal_email'];
    if (!validateAddress($emailVisitor)) {
        $messages['modal_email'] = "You have to insert valid email address.";
    }

    if (sizeof($messages) === 0 && $id > 0) {
        $dataEmail = [
            'email_visitor' => $emailVisitor
        ];

        $sql5 = "SELECT * FROM visitors WHERE visitors_email = :email_visitor";
        $stmt = $db_connection->prepare($sql5);
        $stmt->execute($dataEmail);
        $data5 = $stmt->fetch(PDO::FETCH_ASSOC);        

        if ($data5 === false) {
            $sql6 = "INSERT INTO visitors (visitors_email) VALUES (:email_visitor)";
            $stmt = $db_connection->prepare($sql6);
            try {
                $stmt->execute($dataEmail);
                $_SESSION['visitor'] = $emailVisitor; 
                header('Location: single-page.php?id='.$id);
                exit();
            } catch (PDOException $e) {
                $failed = true;
                $messages['fail'] = "Something unexpected happened.";
            }
        } else {
            $_SESSION['visitor'] = $data5['visitors_email']; 
            header('Location: single-page.php?id='.$id);
            exit();
        }
    }
}

?>

<!DOCTYPE html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/navbar-style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <title>Brainster</title>
</head>

<body>
    <header class="nav-header navbar-fixed-top nav-header-background-main">
        <nav>
            <ul class="nav-links">
                <li class="list-item"><a class="menu-btn" href=# type="button" class="btn btn-primary btn-lg"
                        data-toggle="modal" data-target="#modal"><i class="fas fa-bars"></i> Мени</a></li>
            </ul>
        </nav>
        <img id="logo" class="logo scroll-logo" src="images/logo.png" alt="logo">
        <div class="created-by-text-responsive white">Изработено од студентите на академијата за програмирање <a
                class="underline" href="https://brainster.co/" target="_blank">Brainster.</a></div>
        <div class="cta">
            <a href="https://brainster.co/business" target="_blank" class="button yellow-btn btn-md">Обуки за
                компании</a>
            <a href="register.php" target="_blank" class="button black-btn btn-md">Вработи наши
                студенти</a>
        </div>
    </header>
    <div id="cut1"></div>
    <div class="container-fluid" id="top-container">
        <div class="row" id="top-row">
            <div class="container">
                <div class="row">
                    <div class="created-by-text col-md-12 white">Изработено од студентите на академијата за програмирање
                        <a class="underline" href="https://brainster.co/" target="_blank">Brainster.</a></div>
                    <div class="col-md-12 white text-center">
                        <h1 class="future-title">FUTURE-PROOF YOUR ORGANIZATION</h1>
                        <div class="row">
                            <div class="col-md-3 col-sm-1 col-xs-1"></div>
                            <div class="col-md-6 col-sm-10 col-xs-10">
                                <p class="paragraph-text">Find out how to unlock progress in your business. Understand
                                    your current state,
                                    identify opportunity areas
                                    and prepare to take charge of your organization's future.
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-1 col-xs-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center assesment">
                        <br>
                        <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"
                            class="btn yellow-btn btn-md button assesment-btn">Take the assessment
                        </a>
                    </div>
                </div>
                <div id="outer-filter-section">
                    <div class="row categories-section" id="filter-section">
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="row">
                                <h5 class="group-section-title">Browse by Category</h5>
                            </div>
                            <div class="row category-container">
                                <?php foreach ($data2 as $row) { ?>     
                                <?php  $cat_total += $row['total'] ?>  
                                <?php if ($row['category_name']) { ?>                  
                                <button class="category-btn <?= $category && intval($category) === intval($row["id"]) ? 'category-active': '' ?>" data-id="<?= $row['id'] ?>"><?= $row["category_name"] ?> <span
                                        class="text-muted">(<?= $row['total'] ?>)</span></button>                           
                                <?php } ?>
                                <?php } ?>
                                <button id="category-all" class="category-btn <?= !$category ? 'category-active' : '' ?>" data-id="">ALL <span
                                        class="text-muted">(<?= $cat_total ?>)</span></button>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-7">
                            <div class="row">
                                <h5 class="group-section-title">Time frame (minutes)</h5>
                            </div>
                            <div class="row category-container">
                                <?php foreach ($data3 as $row) { ?>
                                <button class="duration-btn <?= $duration && intval($duration) === intval($row["id"]) ? 'duration-active': '' ?>" data-id="<?= $row['id'] ?>"><?= $row['duration_time'] ?></button>                        
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-5">
                            <div class="row">
                                <h5 class="group-section-title">Group Size</h5>
                            </div>
                            <div class="row category-container">
                            <?php foreach ($data4 as $row) { ?>
                                <button class="group-size-btn <?= $group_size && intval($group_size) === intval($row["id"]) ? 'group-size-active': '' ?>" data-id="<?= $row['id'] ?>"><?= $row['members'] ?></button>                        
                            <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="row">
                                <h5 class="group-section-title" id="search-btn-whitespace">&nbsp;</h5>
                            </div>
                            <div class="row category-container" id="filter-btn">
                                <form class="form" method="POST" action="">
                                    <button class="search-category-btn" type="submit" name="search">SEARCH</button>
                                    <input type="hidden" value="<?= $category ? $category : ''  ?>" id="category-field" name="category">
                                    <input type="hidden" value="<?= $duration ? $duration : '' ?>" id="duration-field" name="duration">
                                    <input type="hidden" value="<?= $group_size ? $group_size : '' ?>" id="group-size-field" name="group_size">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row cards">                    
                    <?php foreach ($data1 as $row) { ?>
                    <div class="col-md-4 col-sm-6 col-xs-12" id="game-card">
                        <div class="thumbnail" id="my-thumbnail">
                            <a href="<?= isset($_SESSION['visitor']) ? "single-page.php?id=".$row['id'] : "" ?>" class="card-url" data-id=<?= $row['id'] ?> <?= !isset($_SESSION['visitor']) ? 'data-toggle="modal" data-target="#email-modal"' : "" ?> ><img class="big-thumbnail-picture" src="images/cards-big/<?= $row['big_picture'] ?>" height="250px" alt="thumbnail-picture"></a>
                            <div class="caption thumbnail-caption">
                                <div class="title-div">
                                    <div>
                                        <a href="<?= isset($_SESSION['visitor']) ? "single-page.php?id=".$row['id'] : "" ?>" class="card-url black-link" data-id=<?= $row['id'] ?> <?= !isset($_SESSION['visitor']) ? 'data-toggle="modal" data-target="#email-modal"' : "" ?> >
                                            <h3 class="thumbnail-label"><?= $row['title'] ?></h3>
                                        </a>
                                        <p>Category: <span class="aqua"><?= $row['category_name'] ?></span></p>
                                    </div>
                                    <a href="<?= isset($_SESSION['visitor']) ? "single-page.php?id=".$row['id'] : "" ?>" class="card-url black-link" data-id=<?= $row['id'] ?> <?= !isset($_SESSION['visitor']) ? 'data-toggle="modal" data-target="#email-modal"' : "" ?> >
                                        <img src="images/cards-small/<?= $row['small_picture'] ?>" class="img-responsive small-thumbnail-picture">
                                    </a>
                                </div>
                                <div class="time">
                                    <i class="far fa-clock fa-2x"></i>&nbsp;
                                    <div> <span id="time-duration">Duration</span>
                                        <div class="text-muted"><?=$row['duration_time']?> minutes</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>                                       
                </div>                
            </div>                
        </div>
        <div class="row">        
            <div id="cut2">
            <div id="cut2-purple"></div>
                <div class="container" id="bottom-title">
                    <div class="col-md-12 white text-center">
                        <h1 class="future-title">FUTURE-PROOF YOUR ORGANIZATION</h1>
                        <div class="row">
                            <div class="col-md-3 col-sm-1 col-xs-1"></div>
                            <div class="col-md-6 col-sm-10 col-xs-10">
                                <p class="paragraph-text">Find out how to unlock progress in your business. Understand
                                    your current state,
                                    identify opportunity areas
                                    and prepare to take charge of your organization's future.
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-1 col-xs-1"></div>
                        </div>
                    </div>
                        <div class="col-md-12 text-center">
                            <br>                        
                            <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"
                                class="btn yellow-btn btn-md button assesment-btn">Take the assessment</a>
                        </div>
                    </div>
                </div>                          
            </div>           
            <div class="row">
                <footer>
                    <div class="col-sm-12 col-xs-12 footer-filter text-center" id="footer-1">
                        <div class="footer-categories-section collapse" id="collapseButton">

                        </div>
                        
                        <button class="btn filter-custom-btn " type="button" data-toggle="collapse" data-target="#collapseButton" aria-expanded="false" aria-controls="collapseExample">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                            <br>
                            <span>Cards Filter</span>                                                        
                        </button>                        
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" id="footer-2">
                        <ul class="footer-list" id="first-footer-list">
                            <li><a class="white-list" href="about.php">About Us</a></li>
                            <li><a class="white-list" href="contact.php">Contact</a></li>
                            <li><a class="white-list" href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank">Gallery</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" id="footer-3">
                        <img  id="logo-bottom" src="images/logo.png" class="bottom-logo scroll-logo">
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" id="footer-4">
                        <ul class="footer-list" id="second-footer-list">
                            <li><a href="https://www.linkedin.com/school/brainster-co/" target="_blank" class="footer-icon"><i class="fab fa-linkedin-in fa-2x"></i></a></li>
                            <li><a href="https://twitter.com/BrainsterCo" target="_blank" class="footer-icon"><i class="fab fa-twitter fa-2x"></i></a></li>
                            <li><a href="https://www.linkedin.com/school/brainster-co/" target="_blank" class="footer-icon"><i class="fab fa-facebook-f fa-2x"></i></a></li>
                        </ul>
                    </div>    
                </footer>  
            </div>                 
        </div>       
    </div>  

   <!--Side-Modal-->

   <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog my-modal" role="document">
            <div class="modal-content my-modal">
                <header class="side-header">
                    <img src="images/logo.png" class="side-img">
                    <button class="close-button" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"><img src="images/close.svg" class="close-img"> Затвори
                        </span></button>
                </header>
                <div class="modal-body side-list">
                    <div class="list-group">
                        <a href="register.php" target="_self" class="list-group-item yellow-line">Регистрирај се</a>
                        <a href="#" class="list-group-item yellow-line" target="_self">Најави се</a>                        
                        <a href="about.php" class="list-group-item black-line" target="_self">За нас</a>
                        <a href="https://www.facebook.com/pg/brainster.co/photos/" class="list-group-item black-line" target="_blank">Галерија</a>
                        <a href="contact.php"target="_self" class="list-group-item black-line">Контакт</a>
                    </div>
                </div>
            </div>
        </div>
    </div>      

   <!-- No data modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="no-data-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content my-no-data-modal">
                <div class="modal-header no-data-modal-header">
                    &nbsp;              
                </div>
                <div class="modal-body">
                    <h2><?= isset($messages['fail']) ? $messages['fail'] : "Не се пронајдени податоци." ?></h2>
                </div>
                <div class="modal-footer no-data-modal-footer">
                    <button type="button" class="btn btn-md btn-custom" data-dismiss="modal"> Close</button>  
                </div>        
            </div>
        </div>
    </div>  
    
   <!-- email-modal -->   

   <div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog text-center" role="document">
            <div class="modal-content my-email-modal">
                <div class="modal-header my-email-modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <label for="modal-input"><h4 class="modal-title" id="myModalLabel">Insert your e-mail</h4></label>
                </div>
                <div class="modal-body">
                        <form action="" method="POST" id="mail-form">
                            <input type="text" title ="Please insert your valid email." class="modal-input" id="modal-input" placeholder="e-mail" name="modal_email">                           
                            <br>
                            <span id="validate" class="text-danger"></span>
                            <br>
                            <button type="submit" id="go-button" class="btn btn-custom" name="modal_submit">GO</button>
                            <input type="hidden" value="" id="card-id" name="card_id">
                        </form>
                </div>
                <div class="my-email-modal-footer">                   
                    &nbsp;
                </div>
            </div>
        </div>
    </div> 

    <!-- No valid email modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="no-valid-mail">
        <div class="modal-dialog" role="document">
            <div class="modal-content my-no-data-modal">
                <div class="modal-header no-data-modal-header"> 
                    &nbsp;
                </div>
                <div class="modal-body">
                    <span class="text-danger"><?php echo isset($messages['modal_email']) ? $messages['modal_email'] : "" ?></span>
                </div>
                <div class="modal-footer no-data-modal-footer">
                    <button type="button" class="btn btn-md btn-custom" data-dismiss="modal"> Close</button>  
                </div>        
            </div>
        </div>
    </div>   
        
    <script src="script/jquery-3.4.1.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>
    <script src="script/jquery-index.js"></script>
    <script>      
        jQuery(document).ready(function(){
            jQuery('.scroll-logo').click(function () {
                jQuery('body,html').animate({
                    scrollTop: 0
                }, 400);
                return false;
            });
        });

        <?php if (sizeof($data1) === 0 || $failed) { ?>
            jQuery('#no-data-modal').modal('toggle');
        <?php } ?>  
     
        <?php if (isset($emailVisitor) && !validateAddress($emailVisitor)) { ?>
            jQuery('#no-valid-mail').modal('toggle');
        <?php } ?>      
       
        document.getElementById('go-button').addEventListener('click', submitEmail);        
        document.getElementById('modal-input').addEventListener('focus', function() {
            let invalid = document.getElementById('validate');
            invalid.innerHTML = '';
            invalid.style.display = 'none';
        });

        jQuery('#email-modal').on('hide.bs.modal', function () {
            let invalid = document.getElementById('validate');
            invalid.innerHTML = '';
            invalid.style.display = 'none';
        });

        function submitEmail(e) {
            let yourEmail = document.getElementById('modal-input');
            let mailFormat = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i;
            let invalid = document.getElementById('validate');
            
            if (yourEmail.value === "" || yourEmail.value <= 0 || !yourEmail.value.match(mailFormat)) {
                e.preventDefault();
                invalid.innerHTML = 'Please insert valid email.';
                invalid.style.display = 'block';
                return;
            }
        }
        
    </script>
    
</body>

</html>
