<?php
    function validateInput($string) {
        return trim(htmlspecialchars($string));
    }

    function checkLength($string) {
        return strlen($string) > 0;
    }

    function validateAddress($string) {
        return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i",$string);
    }

    function validatePhone($string) {
        return preg_match("/^[0-9]*$/",$string);
    }

    function checkMax($string,$limit) {
        return strlen($string) <= $limit;
    }
?>

