<?php
require_once 'db.php';
require_once 'validation.php';
    
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        $message = [];        
       
        $name = validateInput($_POST['name-area']); 
        if (!checkLength($name)) {
            $message['name-area'] = "This field is required.";
        }     

        $company = validateInput($_POST['company-area']);
        if (!checkLength($company)) {
            $message['company-area'] = "This field is required.";
        }

        $phone = validateInput($_POST['phone-area']);
        if (!checkLength($phone)) {
            $message['phone-area'] = "This field is required.";
        } else if (!validatePhone($_POST['phone-area'])) {
            $message['phone-area'] = "This field must contain only numbers.";
        }

        $email = validateInput($_POST['email-area']);
        if (!checkLength($email)) {
            $message['email-area'] = "This field is required.";
        } else if (!validateAddress($email)) {
            $message['email-area'] = "You have to insert valid email address.";
        }

        $text = validateInput($_POST['text-area']);
        if (!checkLength($text)) {
            $message['text-area'] = "This field is required.";
        } else if (!checkMax($text, 255)) {
            $message['text-area'] = "The maximum length for this field is 255 characters.";
        }

        if (count($message) === 0 ) {
          
            $data = [
                ':name' => $name,
                ':company' => $company,
                ':phone' => $phone,
                ':email' => $email,
                ':message' => $text
            ];

            $sql = "INSERT INTO contact (name, company, phone, email, message) VALUES (:name, :company, :phone, :email, :message)";
            $stmt = $db_connection->prepare($sql);
       
            try {
                $stmt->execute($data);
                $message['success'] = "Your informations has been sent successfuly."; 
                unset($_POST);                  
            } catch (PDOEException $e) {
                $message['fail'] = "Something bad happened.";
            }        
        };        
    }
       
?>

<!DOCTYPE html>
<html>

<head>
    <title>Brainster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/navbar-style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
</head>
<body class="body-contact">
    <header class="nav-header navbar-fixed-top contact-header-background">
        <nav>
            <ul class="nav-links">
                <li class="list-item"><a class="menu-btn" href=# type="button" class="btn btn-primary btn-lg"
                        data-toggle="modal" data-target="#modal"><i class="fas fa-bars"></i> Мени</a></li>
            </ul>
        </nav>
        <img id="logo" class="logo scroll-logo" src="images/logo.png" alt="logo">
        <div class="cta">
            <a href="https://brainster.co/business" target="_blank" class="button yellow-btn btn-md">Обуки за
                компании</a>
            <a href="register.php" target="_blank" class="button black-btn btn-md">Вработи наши
                студенти</a>
        </>
    </header>
    <div class="faded">
        <div class="container"> 
            <?php if (isset($message['success'])) { ?>
                <div class="alert my-alert" role="alert"><?php echo $message['success'] ?> </div>
            <?php } else if (isset($message['fail'])) { ?>
                <div class="alert my-alert" role="alert"><?php echo $message['fail'] ?> </div>
             <?php } ?>         
            <div class="contact-box">                
                <div class="row">                
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="h2 title-contact text-center">Contact</div>
                        <div class="contact-elements">
                            <p class="contact-section"><i class="fas fa-map-marker-alt fa-2x"></i> &nbsp; <span class="text-size">Kosturski Heroi 28, Skopje</span></p>
                            <p class="contact-section"><i class="fas fa-envelope fa-2x"></i> &nbsp; <span class="text-size">contact@brainster.co</span></p>
                            <p class="contact-section"><i class="fas fa-phone-alt fa-2x"></i> &nbsp; <span class="text-size">Call 070 384 728</span></p> 
                            <p class="contact-section"><i class="fas fa-globe fa-2x"></i> &nbsp; <a href="https://brainster.co/" target="_blank"><span class="text-size">https://brainster.co</span></a></p>                       
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <form action="" method="POST" id="contact-form">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control my-input" id="name" placeholder="Your name" name="name-area" value="<?php echo isset($_POST['name-area']) ? $_POST['name-area'] : "" ?>">
                                <span class="text-danger" id="name-span"><?php echo isset($message['name-area']) ? $message['name-area'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="company-name">Company:</label>
                                <input type="text" class="form-control my-input" id="company-name" placeholder="Company name" name="company-area" value="<?php echo isset($_POST['company-area']) ? $_POST['company-area'] : "" ?>">
                                <span class="text-danger" id="company-span"><?php echo isset($message['company-area']) ? $message['company-area'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="telephone">Phone:</label>
                                <input type="text" class="form-control my-input" id="telephone" placeholder="Phone" name="phone-area" value="<?php echo isset($_POST['phone-area']) ? $_POST['phone-area'] : "" ?>">
                                <span class="text-danger" id="phone-span"><?php echo isset($message['phone-area']) ? $message['phone-area'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="email-address">Email address:</label>
                                <input type="email" class="form-control my-input" id="email-address" placeholder="Email" name="email-area" value="<?php echo isset($_POST['email-area']) ? $_POST['email-area'] : "" ?>">
                                <span class="text-danger" id="email-span"><?php echo isset($message['email-area']) ? $message['email-area'] : "" ?></span>
                            </div>
                            <div class="form-group">
                                <label for="message">Message:</label>
                                <textarea class="form-control my-input" id="message" rows="6" placeholder="Your Message" name="text-area" value="<?php echo isset($_POST['text-area']) ? $_POST['text-area'] : "" ?>"></textarea>
                                <span class="text-danger" id="message-span"><?php echo isset($message['text-area']) ? $message['text-area'] : "" ?></span>
                            </div>
                            <div class="button-div text-center">
                                <button id="submit-btn" type="submit" class="custom-contact-btn btn">Send</button>
                            </div>
                        </form>
                    </div>
                </div>               
            </div>
            <div class="row">
                <div class="col-md-12 text-center footer text-size">
                    <p>За да се вратиш на почетната страница <a class="click" href="index.php">Кликни тука.</a></p>
                </div>
            </div>    
        </div>
    </div>


    <!--Modal-->

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog my-modal" role="document">
            <div class="modal-content my-modal">
                <header class="side-header">
                    <img src="images/logo.png" class="side-img">
                    <button class="close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img
                                src="images/close.svg" class="close-img"> Затвори
                        </span></button>
                </header>
                <div class="modal-body side-list">
                    <div class="list-group">
                        <a href="register.php" target="_self" class="list-group-item yellow-line">Регистрирај се</a>
                        <a href="#" target="_self" class="list-group-item yellow-line">Најави се</a>
                        <a href="about.php" target="_self" class="list-group-item black-line">За нас</a>
                        <a href="https://www.facebook.com/pg/brainster.co/photos/"
                            class="list-group-item black-line" target="_blank">Галерија</a>
                        <a href="contact.php"target="_self" class="list-group-item black-line">Контакт</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="script/jquery-3.4.1.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>
    <script>
        jQuery('#name').on('focus', function() {
          jQuery('#name-span').text('');
        });

        jQuery('#company-name').on('focus', function() {
           jQuery('#company-span').text('');
        });

        jQuery('#telephone').on('focus', function() {
            jQuery('#phone-span').text('');
        });

        jQuery('#email-address').on('focus', function() {
           jQuery('#email-span').text('');
        });

        jQuery('#message').on('focus', function() {
            jQuery('#message-span').text('');
        });      

    </script>
</body>
</html>