<?php

    require_once 'db.php';

    $sql = "SELECT u.name, u.surname, u.company, u.email_address, u.phone, e.employees, s.sectors FROM users u, employees_number e, sector s WHERE u.employees_fk = e.id AND u.sector_fk = s.id";
    $data = $db_connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>

<head>
    <title>Brainster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/navbar-style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
</head>
<body class="body-admin">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
               <a href="index.php">
                  <img src="images/logo.png" class="admin-logo">
               </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-left h1">Admin panel</div>            
        </div>
        <div class="row">
            <div class="col-md-12 admin-table table-responsive">           
                <table class="table table-striped my-table">
                    <thead>
                        <tr>
                          <th scope="col" class="h4">First Name</th>
                          <th scope="col" class="h4">Last Name</th>
                          <th scope="col" class="h4">Company</th>
                          <th scope="col" class="h4">Email</th>
                          <th scope="col" class="h4">Phone</th>
                          <th scope="col" class="h4">Employees #</th>
                          <th scope="col" class="h4">Sector</th>
                        </tr>
                    </thead>
                    <?php foreach ($data as $row) { ?>
                    <tbody>                                       
                        <tr>
                          <td><?= $row['name']?></td>
                          <td><?= $row['surname']?></td>
                          <td><?= $row['company']?></td>
                          <td><?= $row['email_address']?></td>
                          <td><?= $row['phone']?></td>
                          <td><?= $row['employees']?></td>
                          <td><?= $row['sectors']?></td>                        
                        </tr>                     
                    </tbody>    
                    <?php } ?>                
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center footer text-size">
                <p>За да се вратиш на почетната страница <a class="click" href="index.php">Кликни тука.</a></p>
            </div>
        </div>  
    </div>

</body>
    <script src="script/jquery-3.4.1.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>

</html>