-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 09, 2020 at 10:56 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Project02`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(10) UNSIGNED NOT NULL,
  `big_picture` varchar(32) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `small_picture` varchar(32) DEFAULT NULL,
  `category_fk` int(10) UNSIGNED DEFAULT NULL,
  `group_size_fk` int(10) UNSIGNED NOT NULL,
  `duration_fk` int(10) UNSIGNED NOT NULL,
  `facility_fk` int(10) UNSIGNED NOT NULL,
  `cards_content` varchar(32) DEFAULT NULL,
  `materials` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `big_picture`, `title`, `small_picture`, `category_fk`, `group_size_fk`, `duration_fk`, `facility_fk`, `cards_content`, `materials`) VALUES
(1, '1.jpg', 'Dotmocracy', '1.png', 4, 3, 1, 1, '1.html', 'Стикери,\nПенкала /маркери'),
(2, '2.jpg', 'Project Mid-way Evaluation', '2.png', 4, 1, 2, 2, '2.html', 'Стикери,\nПенкала/Маркери,\nФлипчарт'),
(4, '3.jpg', 'The 5 Whys', '3.png', 2, 1, 2, 1, '3.html', 'Бела табла или Флипчарт, листови'),
(5, '4.jpg', 'Future Trends', '4.png', 2, 2, 3, 2, '4.html', 'Пенкала,\nБела табла или Флипчарт,\nМаркери,\nСелотејп,'),
(6, '5.jpg', 'Story Building', '5.png', 2, 2, 2, 1, '5.html', 'Лаптоп,\nТаблет,\nСмартфон'),
(7, '6.jpg', 'Take a Stand', '6.png', 2, 2, 3, 2, '6.html', ''),
(8, '7.jpg', 'IDOARRT Meeting Design', '7.png', 4, 3, 1, 1, '7.html', 'Табла за пишување,\nФлипчарт'),
(9, '8.jpg', 'Action Steps', '8.png', 4, 1, 4, 2, '8.html', 'Пенкала/Маркери во боја\nА3 формат, Хартија '),
(10, '9.jpg', 'Letter to Myself', '9.png', NULL, 4, 1, 1, '9.html', 'Пенкало,\nРазгледница,\nХартија,\nКоверт,\nМаркери,\nФлипчарт'),
(11, '10.jpg', 'Active Listening', '10.png', 3, 2, 3, 2, '10.html', 'Пенкала/Маркери, \nФлипчарт'),
(12, '11.jpg', 'Feedback I appreciate', '11.png', 3, 2, 3, 3, '11.html', 'Стикери,\nПенкала'),
(13, '12.jpg', 'Explore your values', '12.png', 3, 2, 3, 2, '12.html', 'Стикери, Пенкала '),
(14, '13.jpg', 'Reflection Individual', '13.png', 3, 2, 2, 2, '13.html', 'Тефтер, Лист,\nПенкало '),
(15, '14.jpg', 'Back-turned Feedback', '14.png', 3, 2, 3, 3, '14.html', 'Пенкало, Маркери, \nСтикери '),
(16, '15.jpg', 'Conflict Responses', '15.png', 5, 1, 3, 1, '15.html', 'Method Kit за тимови,\nСтикери,\nМаркери'),
(17, '16.jpg', 'Myers-Briggs Team Reflection', '16.png', 5, 1, 3, 1, '16.html', ''),
(18, '17.jpg', 'Personal Presentations', '17.png', 5, 2, 4, 2, '17.html', 'Материјали,\nФлипчарт,\nМаркери'),
(19, '18.jpg', 'Circles of influence', '18.png', NULL, 4, 5, 1, '18.html', 'Маркери во боја,\nФлипчарт,\nБела табла ');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(33) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(2, 'INNOVATION'),
(3, 'SELF-LEADERSHIP'),
(4, 'ACTION'),
(5, 'TEAM'),
(6, 'ENERGIZERS');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `duration`
--

CREATE TABLE `duration` (
  `id` int(10) UNSIGNED NOT NULL,
  `duration_time` varchar(33) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `duration`
--

INSERT INTO `duration` (`id`, `duration_time`) VALUES
(1, '5-30'),
(2, '30-60'),
(3, '60-120'),
(4, '120-240'),
(5, '30-120');

-- --------------------------------------------------------

--
-- Table structure for table `employees_number`
--

CREATE TABLE `employees_number` (
  `id` int(10) UNSIGNED NOT NULL,
  `employees` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees_number`
--

INSERT INTO `employees_number` (`id`, `employees`) VALUES
(1, '1'),
(2, '2-10'),
(3, '11-50'),
(4, '51-200'),
(5, '200+');

-- --------------------------------------------------------

--
-- Table structure for table `facility_level`
--

CREATE TABLE `facility_level` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `facility_level`
--

INSERT INTO `facility_level` (`id`, `level`) VALUES
(1, 'Почетно'),
(2, 'Средно'),
(3, 'Напредно');

-- --------------------------------------------------------

--
-- Table structure for table `group_size`
--

CREATE TABLE `group_size` (
  `id` int(10) UNSIGNED NOT NULL,
  `members` varchar(33) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_size`
--

INSERT INTO `group_size` (`id`, `members`) VALUES
(1, '2-10'),
(2, '2-40'),
(3, '3-40'),
(4, '2-40+');

-- --------------------------------------------------------

--
-- Table structure for table `sector`
--

CREATE TABLE `sector` (
  `id` int(10) UNSIGNED NOT NULL,
  `sectors` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sector`
--

INSERT INTO `sector` (`id`, `sectors`) VALUES
(1, 'HR'),
(2, 'Marketing'),
(3, 'Production'),
(4, 'Sales'),
(5, 'CEO'),
(6, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(122) DEFAULT NULL,
  `surname` varchar(122) DEFAULT NULL,
  `company` varchar(122) DEFAULT NULL,
  `email_address` varchar(122) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `message` text NOT NULL,
  `employees_fk` int(10) UNSIGNED NOT NULL,
  `sector_fk` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `visitors_email` varchar(122) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_cards` (`category_fk`),
  ADD KEY `group_size_cards` (`group_size_fk`),
  ADD KEY `duration_cards` (`duration_fk`),
  ADD KEY `facility_cards` (`facility_fk`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `duration`
--
ALTER TABLE `duration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees_number`
--
ALTER TABLE `employees_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_level`
--
ALTER TABLE `facility_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_size`
--
ALTER TABLE `group_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_number_users` (`employees_fk`),
  ADD KEY `sector_users` (`sector_fk`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `duration`
--
ALTER TABLE `duration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employees_number`
--
ALTER TABLE `employees_number`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `facility_level`
--
ALTER TABLE `facility_level`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `group_size`
--
ALTER TABLE `group_size`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sector`
--
ALTER TABLE `sector`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `category_cards` FOREIGN KEY (`category_fk`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `duration_cards` FOREIGN KEY (`duration_fk`) REFERENCES `duration` (`id`),
  ADD CONSTRAINT `facility_cards` FOREIGN KEY (`facility_fk`) REFERENCES `facility_level` (`id`),
  ADD CONSTRAINT `group_size_cards` FOREIGN KEY (`group_size_fk`) REFERENCES `group_size` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `employees_number_users` FOREIGN KEY (`employees_fk`) REFERENCES `employees_number` (`id`),
  ADD CONSTRAINT `sector_users` FOREIGN KEY (`sector_fk`) REFERENCES `sector` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
