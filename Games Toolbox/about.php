<!DOCTYPE html>
<html>

<head>
    <title>Brainster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/navbar-style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
</head>
<body class="body-about">
    <header class="nav-header navbar-fixed-top contact-header-background">
        <nav>
            <ul class="nav-links">
                <li class="list-item"><a class="menu-btn" href=# type="button" class="btn btn-primary btn-lg"
                        data-toggle="modal" data-target="#modal"><i class="fas fa-bars"></i> Мени</a></li>
            </ul>
        </nav>
        <img id="logo" class="logo scroll-logo" src="images/logo.png" alt="logo">
        <div class="cta">
            <a href="https://brainster.co/business" target="_blank" class="button yellow-btn btn-md">Обуки за
                компании</a>
            <a href="register.php" target="_blank" class="button black-btn btn-md">Вработи наши
                студенти</a>
        </div>
    </header>
    <div class="faded">
        <div class="container">
            <div class="box">
                <div class="row">
                    <div class="col-md-12 h1 title">Our Story</div>
                    <div class='col-md-12'>
                        <p> Brainster е иновативна компанија за едукација каде можеш да научиш практични вештини за
                            трансформација на кариерата. Нашата специјалност е организирање на курсеви, академии, кариерна
                            трансформација и поврзување на талентите со иновативните компании oд областа на 
                            <span class="colored"> маркетинг, дизајн,
                                бизнис и програмирање.
                            </span>
                        </p>
                        <p>
                            Во изминатите 3 години Brainster реализираше повеќе од 400 курсеви преку кои 5000 учесници се
                            стекнаа со нови вештини и стигнаа чекор поблиску до кариерата која ја посакуваат.
                            Нашата мисија е да им помогнеме на 1 милион индивидуалци да стигнат до кариера која секогаш ја
                            посакувале.
                        </p>
                        <p>
                            <span class="colored h5">Курсеви:</span>
                            <a href="https://brainster.co/courses" target="_blank">https://brainster.co/courses</a>
                        </p>   
                        <p>
                            <span class="colored h5">Академија за маркетинг:</span> 
                            <a href="https://marketpreneurs.brainster.co/" target="_blank"> https://marketpreneurs.brainster.co</a>
                        </p>                   
                        <p>
                            <span class="colored h5">Академија за дизајн:</span> 
                            <a href="https://design.brainster.co/" target="_blank">https://design.brainster.co</a>
                        </p>
                        <p>
                            <span class="colored h5">Академија за програмирање:</span> 
                            <a href="http://codepreneurs.co/" target="_blank">http://codepreneurs.co</a>
                        </p>
                        <p>
                            <span class="colored h5">Академија за Data Science:</span> 
                            <a href="https://datascience.brainster.co/" target="_blank"> https://datascience.brainster.co</a>
                        </p>
                        <p>
                            <span class="colored h5">Академија за Software testing:</span> 
                            <a href="https://qa.brainster.co/" target="_blank"> https://qa.brainster.co</a>
                        </p>
                        <p>
                            <span class="colored h5">Академија за UX/UI Design:</span> 
                            <a href="https://ux.brainster.co/" target="_blank"> https://ux.brainster.co</a>
                        </p>                        
                    </div>                
                </div>                                   
            </div>  
            <div class="row">
                <div class="col-md-12 text-center footer">
                    <p>За да се вратиш на почетната страница <a class="click" href="index.php">Кликни тука.</a></p>
                </div>
            </div>                     
        </div>
    </div>


    <!--Modal-->

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog my-modal" role="document">
            <div class="modal-content my-modal">
                <header class="side-header">
                    <img src="images/logo.png" class="side-img">
                    <button class="close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img
                                src="images/close.svg" class="close-img"> Затвори
                        </span></button>
                </header>
                <div class="modal-body side-list">
                    <div class="list-group">
                        <a href="register.php" target="_self" class="list-group-item yellow-line">Регистрирај се</a>
                        <a href="#" class="list-group-item yellow-line" target="_self">Најави се</a>
                        <a href="about.php" class="list-group-item black-line" target="_self">За нас</a>
                        <a href="https://www.facebook.com/pg/brainster.co/photos/"
                            class="list-group-item black-line" target="_blank">Галерија</a>
                        <a href="contact.php" target="_self" class="list-group-item black-line">Контакт</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="script/jquery-3.4.1.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>
</body>
</html>