<?php
    $cards = array(
        array(
            "thumbnail" => "1.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "2.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "3.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "4.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "5.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "6.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "7.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "8.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "9.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "10.jpg",
            "text" => "Академија за програмирање",
            "class" => "programming"
        ),
        array(
            "thumbnail" => "11.jpg",
            "text" => "Академија за маркетинг",
            "class" => "marketing"
        ),
        array(
            "thumbnail" => "12.jpg",
            "text" => "Академија за маркетинг",
            "class" => "marketing"
        ),
        array(
            "thumbnail" => "13.jpg",
            "text" => "Академија за маркетинг",
            "class" => "marketing"
        ),
        array(
            "thumbnail" => "14.jpg",
            "text" => "Академија за маркетинг",
            "class" => "marketing"
        ),
        array(
            "thumbnail" => "15.jpg",
            "text" => "Академија за маркетинг",
            "class" => "marketing"
        ),
        array(
            "thumbnail" => "16.jpg",
            "text" => "Академија за маркетинг",
            "class" => "marketing"
        ),
        array(
            "thumbnail" => "17.jpg",
            "text" => "Академија за дизајн",
            "class" => "design"
        ),
        array(
            "thumbnail" => "18.jpg",
            "text" => "Академија за дизајн",
            "class" => "design"
        ),
        array(
            "thumbnail" => "19.jpg",
            "text" => "Академија за дизајн",
            "class" => "design"
        ),
        array(
            "thumbnail" => "20.jpg",
            "text" => "Академија за дизајн",
            "class" => "design"
        )    
    );
?>
<!DOCTYPE html>
<html>
<head>
    <title>Brainster Labs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/navbar-style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-default navbar-shadow">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="index.php"><img src="./images/brainster-logo.png" alt="Brainster" class="img-responsive">
                    </ahref="#">
            </div>
            <div class="navbar-right menu-btn">
                <a href="#" onclick="openNav()"><img src="./images/responsive-menu.png" alt="Menu"
                        class="img-responsive"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="navbar-collapsing-area">
                <!-- Button to close the overlay navigation -->
                <a href="javascript:void(0)" class="close-btn" onclick="closeNav()">&times;</a>
                <ul class="nav navbar-nav">
                    <li><a href="https://www.brainster.io/marketpreneurs" target="_blank">Академија за маркетинг <span class="sr-only">(current)</span></a>
                    </li>
                    <li><a href="http://codepreneurs.co/" target="_blank">Академија за програмирање</a></li>
                    <li><a href="https://www.brainster.io/design" target="_blank">Академија за дизајн</a></li>
                </ul>
                <form action="form.php" method="post" class="navbar-form navbar-right">
                    <button type="submit" class="btn btn-default">Вработи наш студент</button>
                </form>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="big-picture hidden-xs">
        <div class="big-text">Brainster Labs</div>
    </div>
    <nav>
        <div class="projects-nav">
            <a class="col-md-4 col-sm-12 col-xs-12 project-nav-element marketing-nav">
                <span>Проекти на студентите по академијата за маркетинг</span>
                <i class="glyphicon glyphicon-ok-sign"></i>
            </a>
            <a class="col-md-4 col-sm-12 col-xs-12 project-nav-element programming-nav bordered-sides">
                <span>Проекти на студентите по академијата за програмирање</span>
                <i class="glyphicon glyphicon-ok-sign"></i>
            </a>
            <a class="col-md-4 col-sm-12 col-xs-12 project-nav-element design-nav">
                <span>Проекти на студентите по академијата за дизајн</span>
                <i class="glyphicon glyphicon-ok-sign"></i>
            </a>
        </div>
    </nav>
    <div class="row cards-container">
        <h2 class="text-center"><strong>Проекти</strong></h2>
        <?php foreach ($cards as $card) { ?>
        <div class="col-md-4 col-sm-12 col-xs-12 card-main <?php echo $card['class'] ?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="thumbnail">
                        <div class="thumbnail-img-container">
                            <a href="#">
                                <img src="./images/cards/<?php echo $card['thumbnail'] ?>" alt="Card Image" height="200">
                            </a>
                        </div>
                        <div class="caption">
                            <button class="btn btn-sm btn-warning card-button-up"><strong><?php echo $card['text'] ?></strong></button>
                            <h3><strong>Име на проектот стои овде во две линии</strong></h3>
                            <p>Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.</p>
                            <p><strong>Октомври - Јуни 2019/2020</strong></p>
                            <p class="text-right"><button class="btn btn-danger card-button-down"><strong>Дознај повеќе</strong></button></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center more-less">
            <span class="show-more">Прикажи повеќе<br/><span class="glyphicon glyphicon-menu-down"></span></span>
            <span class="show-less"><span class="glyphicon glyphicon-menu-up"></span><br/>Прикажи помалку</span>
        </div>
    </div>
    <div class="footer">
        <footer class="footer-form text-center">
            <p>Изработено со <i class="glyphicon glyphicon-heart"></i> од студентите на Brainster</p>
        </footer>
    </div>

    <script src="js/jQuery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        var classSelector = '.card-main';

        $('.project-nav-element').on('click', function(e) {
            $('.project-nav-element').removeClass('active');
            $(this).addClass('active');
            
            $('.project-nav-element i').hide();
            $(this).find('i').show();

            if ($(this).hasClass('programming-nav')) {
                classSelector = '.programming';
            } else if ($(this).hasClass('marketing-nav')) {
                classSelector = '.marketing';
            } else if ($(this).hasClass('design-nav')) {
                classSelector = '.design';
            }

            $('.card-main').show();
            if (classSelector) {
                $('.card-main').not(classSelector).hide();
                toggleCards(classSelector);
            }
        });

        toggleCards(classSelector);

        $('.show-more').on('click', function() {
            $(classSelector + ':gt(5)').toggle();
            $('.show-more').hide();
            $('.show-less').show();
        });

        $('.show-less').on('click', function() {
            $(classSelector + ':gt(5)').toggle();
            $('.show-less').hide();
            $('.show-more').show();
        });
        
        function openNav() {
            document.getElementById('navbar-collapsing-area').style.height = '100%';
        }

        function closeNav() {
            document.getElementById('navbar-collapsing-area').style.height = '0';
        }

        function toggleCards(selector) {
            if ($(selector).length > 6) {
                $(selector + ':gt(5)').hide();
                $('.show-more').show();
                $('.show-less').hide();
            } else {
                $('.show-more').hide();
                $('.show-less').hide();
            }
        }
    </script>
</body>
</html>