<?php
  error_reporting(0);
  ini_set('display_errors', 0);

  $message = array("type" => "", "message" => "");
  
  if (isset($_POST['submit_btn'])) {
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $db = "brainster";

    $conn = new mysqli($servername, $username, $password, $db);

    if ($conn->connect_error) {
      $message = array("type" => "form-error-msg", "message" => "Connection to database failed");
    } else {
      $name = mysqli_real_escape_string($conn, trim($_POST['first_last_name']));
      $company = mysqli_real_escape_string($conn, trim($_POST['company']));
      $phone = mysqli_real_escape_string($conn, trim($_POST['phone']));
      $email = mysqli_real_escape_string($conn, trim($_POST['email']));
      $student = mysqli_real_escape_string($conn, trim($_POST['student_type']));

      $sql = "INSERT INTO `messages` (`name`, `company`, `email`, `phone`, `fk_student_type`) VALUES ('$name', '$company', '$email', '$phone', $student)";

      if ($conn->query($sql) === true) {
        $message = array("type" => "form-success-msg", "message" => "Data successfully inserted into database.");
      } else {
        $message = array("type" => "form-error-msg", "message" => "Data insert into database failed.");
      }
    }
    
    $conn->close();
  }
?>

<!DOCTYPE html>

<html>

<head>
  <title>Brainster Labs</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/navbar-style.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <nav class="navbar navbar-default navbar-shadow">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.php"><img src="./images/brainster-logo.png" alt="Brainster" class="img-responsive"></a>
      </div>
      <div class="navbar-right menu-btn">
        <a href="#" onclick="openNav()"><img src="./images/responsive-menu.png" alt="Menu" class="img-responsive"></a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div id="navbar-collapsing-area">
        <!-- Button to close the overlay navigation -->
        <a href="javascript:void(0)" class="close-btn" onclick="closeNav()">&times;</a>
        <ul class="nav navbar-nav">
          <li><a href="https://www.brainster.io/marketpreneurs" target="_blank">Академија за маркетинг <span class="sr-only">(current)</span></a>
          </li>
          <li><a href="http://codepreneurs.co/" target="_blank">Академија за програмирање</a></li>
          <li><a href="https://www.brainster.io/design" target="_blank">Академија за дизајн</a></li>
        </ul>
        <form action="form.php" method="post" class="navbar-form navbar-right">
          <button type="submit" class="btn btn-default">Вработи наш студент</button>
        </form>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <div class="h1 text-center form-title">Вработи студенти</div>
  <div class="container form-container">
    <form class="form" id="contact-form" method="post" action="">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <label for="input1">Име и презиме</label>
          <input type="text" name="first_last_name" id="input1" class="form-control placeholder-text" placeholder="Вашето име и презиме" required pattern="^[a-zA-Z\s]+$">
          <small class="errors" id="error1"></small>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <label for="input2">Име на компанија</label>
          <input type="text" name="company" id="input2" class="form-control placeholder-text" placeholder="Име на вашата компанија" required>
          <small class="errors" id="error2"></small>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <label for="input3">Контакт имејл</label>
          <input type="email" name="email" id="input3" class="form-control placeholder-text"
            placeholder="Контакт имејл на вашата компанија" required>
            <small class="errors" id="error3"></small>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <label for="input4">Контакт телефон</label>
          <input type="text" name="phone" id="input4" class="form-control placeholder-text"
            placeholder="Контакт телефон на вашата компанија" required minlengh="9" maxlength="9" pattern="^07[0-9]{7}$">
            <small class="errors" id="error4"></small>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <label for="input5">Тип на студенти</label>
          <select id="input5" name="student_type" class="form-control select" required>
            <option class="option" value="" disabled selected hidden>Избери тип на студент</option>
            <option class="option" value="1">Студенти од програмирање</option>
            <option class="option" value="2">Студенти од маркетинг</option>
            <option class="option" value="3">Студенти од дизајн</option>
          </select>
          <small class="errors" id="error5"></small>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <label for="input6"><br /></label>
          <button type="submit" name="submit_btn" id="input6" class="form-control btn-red">Испрати</button>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 form-error-container text-center">
          <span class="form-error <?php echo $message['type'] ?>"><?php echo $message['message'] ?>
        </div>
      </div>
    </form>
  </div>
  <div class="footer">
    <footer class="footer-form text-center">
        <p>Изработено со <i class="glyphicon glyphicon-heart"></i> од студентите на Brainster</p>
    </footer>
  </div>

  <script src="js/jQuery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    function openNav() {
      document.getElementById('navbar-collapsing-area').style.height = '100%';
    }

    function closeNav() {
      document.getElementById('navbar-collapsing-area').style.height = '0';
    }
  </script>

</body>

</html>