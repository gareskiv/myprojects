<?php

namespace App;
use App\Card;

use Illuminate\Database\Eloquent\Model;

class Lection extends Model
{
    public function card() {
        return $this->belongsTo(Card::class);
    }
}
