<?php

namespace App;
use App\Visitor;
use App\Card;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function visitors() {
        return $this->belongsTo(Visitor::class, 'category_id', 'id');
    }

    public function card() {
        return $this->hasOne(Card::class);
    }
}
