<?php

namespace App;

use App\Card;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    public function category() {
        return $this->belongsTo(Card::class);
    }
}
