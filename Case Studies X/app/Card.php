<?php

namespace App;

use App\Lection;
use App\Visitor;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{   
    public function visitors()
    {
        return $this->hasMany(Visitor::class);
    }

    public function lections()
    {
        return $this->hasMany(Lection::class);
    }
}
