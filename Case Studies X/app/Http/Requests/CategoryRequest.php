<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoryName' => 'required|max:15',
            'categoryDescription' => 'required|max:400',
            'categoryPicture' => 'max:5000|image|mimes:jpg,png,jpeg,gif,svg|required',  
        ];
    }

    public function messages()
    {
        return [            
            'categoryName.required' => 'Полето е задолжително.',       
            'categoryName.max' => 'Името мора да содржи максимум 15 карактери.',
            'categoryDescription.required' => 'Полето е задолжително.',
            'categoryDescription.max' => 'Описот може да содржи максимум 400 карактери.',
            'categoryPicture.max' => 'Максималната големина на сликата е 5 MБ.',
            'categoryPicture.image' => 'Фајлот мора да биде слика.',
            'categoryPicture.mimes' => 'Валидни формати:jpg,png,jpeg,gif,svg.',
            'categoryPicture.required' => 'Полето е задолжително.', 
        ];
    }
}
