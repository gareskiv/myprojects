<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditLectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lectionTitle' => 'required|max:150',
            'lectionDescription' => 'required',           
        ];
    }

    public function messages()
    {
        return [            
            'lectionTitle.required' => 'Полето е задолжително.',       
            'lectionTitle.max' => 'Името мора да содржи максимум 15 карактери.',
            'lectionDescription.required' => 'Полето е задолжително.',            
        ];
    }
}
