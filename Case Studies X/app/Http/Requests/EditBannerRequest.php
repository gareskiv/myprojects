<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bannerTitle' => 'required|max:150',
            'bannerText' => 'required|max:400',
            'bannerLink' => 'required|url',            
        ];
    }

    public function messages()
    {
        return [            
            'bannerTitle.required' => 'Полето е задолжително.',       
            'bannerTitle.max' => 'Името мора да содржи максимум 150 карактери.',
            'bannerText.required' => 'Полето е задолжително.',
            'bannerText.max' => 'Описот може да содржи максимум 400 карактери.',
            'bannerLink.required' => 'Полето е задолжително.',
            'bannerLink.url' => 'Внесете валиден формат.',
        ];
    }
}
