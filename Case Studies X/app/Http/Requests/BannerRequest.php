<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{   
     /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
           'bannerName' => 'required|max:150',
           'bannerDescription' => 'required|max:250',
           'bannerLink' => 'required|url',
          
        ];
    }

    public function messages()
    {
        return [            
            'bannerName.required' => "Полето е задолжително.",       
            'bannerName.max' => "Името мора да содржи максимум 150 карактери.",
            'bannerDescription.required' => 'Полето е задолжително.',
            'bannerDescription.max' => 'Описот мора да содржи максимум 250 карактери.',
            'baannerDescription' => 'Описот мора да содржи максимум 400 карактери.',
            'bannerLink.required' => "Полето е задолжително.",
            'bannerLink.url' => "Внесете валиден формат.",
        ];
    }
}
