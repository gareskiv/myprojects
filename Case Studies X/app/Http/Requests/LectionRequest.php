<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
        public function rules()
        {
            return [
                'lectionTitle' => 'required|unique:cards,category|max:150',
                'lectionDescription' => 'required',
                'lectionSelect' => 'required',
                'lectionDate' => 'required',            
            ];
        }

        public function messages()
        {
            return [            
                'lectionTitle.required' => 'Полето е задолжително.',   
                'lectionTitle.unique' =>  ' Веќе постои лекција со овој наслов.',             
                'lectionTitle.max' => "Насловот мора да содржи максимум 150 карактери.",
                'lectionDescription.required' => 'Полето е задолжително.',                
                'lectionSelect.required' => 'Полето е задолжително.',          
                'lectionDate.required' => 'Полето е задолжително' ,    
            ];
        }
}
