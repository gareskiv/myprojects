<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            'categoryName' => 'required|max:150',
            'categoryDescription' => 'required|max:400',
            'categoryPicture' => 'max:5000|image|mimes:jpg,png,jpeg,gif,svg',            
        ];
    }

    public function messages()
    {
        return [            
            'categoryName.required' => 'Полето е задолжително.',       
            'categoryName.max' => 'Името мора да содржи максимум 150 карактери.',
            'categoryDescription.required' => 'Полето е задолжително.',
            'categoryDescription.max' => 'Описот мора да содржи максимум 400 карактери.',
            'categoryPicture.max' => 'Максималната големина на сликата е 5 MБ.',
            'categoryPicture.image' => 'Фајлот мора да биде слика.',
            'categoryPicture.mimes' => 'Валидни формати:jpg,png,jpeg,gif,svg.',           
        ];
    }
}
