<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Visitor;
use App\Email;
use App\Card;
use App\Lection;


use Illuminate\Http\Request;


class LandingPageController extends Controller
{
   public function index() 
   {      
      $cards = Card::with('lections')->get();   
      
      return view('landing', compact('cards'));
   }  

   public function registerEmail (Request $request) 
   {
      $input = $request->all();

      $rules = [
         'emailOnly' => 'required|email|unique:visitors,email',
      ];

      $messages = [
         'emailOnly.required' => 'Внесете e-адреса.',
         'emailOnly.email' => 'Внесовте невалиден формат.',
         'emailOnly.unique' => 'Вашата адреса е веќе регистрирана.',
              
      ];

      $validator = Validator::make($input, $rules, $messages);
            
      if ($validator->fails()) {    
         return response()->json($validator->messages());          
      }
      
      $onlyEmail = new Visitor();
      $onlyEmail->email = $input['emailOnly'];      
      $onlyEmail->save();   
      
      return response()->json(['success' => $onlyEmail]); 
   }
}  
