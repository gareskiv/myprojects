<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File; 
use Illuminate\Http\Request;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\LectionRequest;
use App\Http\Requests\BannerRequest;

use App\Http\Requests\EditCategoryRequest;
use App\Http\Requests\EditLectionRequest;
use App\Http\Requests\EditBannerRequest;

use App\Card;
use App\Lection;
use App\Banner;
use App\Visitor;

class AdminController extends Controller
{   
    // category methods

    public function category(CategoryRequest $request) 
    {

        $input = $request->all();

        $category = new Card;

        $file = $request->file('categoryPicture');
        $file->move('images', $file->getClientOriginalName());        

        $category = new Card;
        $category->category = $request->get('categoryName');
        $category->description = $request->get('categoryDescription');  
        $category->image = $file->getClientOriginalName();        
        $category->save();

        $session = \Session::flash('successCategory');

        return back();
    }

    public function addCategory() 
    {

        $cards = Card::with('lections')->get();

        return view('actions.add-category', compact('cards'));
    }

    public function categoryEdit($id) 
    {

        $cards = Card::with('lections')->get();
        $singleCard = Card::where('id', $id)->first();

        if(!$singleCard) {
            return redirect("/");
        }

        return view('edit.category-edit', compact('cards', 'singleCard'));
    }

    public function categoryUpdate(EditCategoryRequest $request, $id) 
    {

        $category = Card::where('id', $id)->first();

        if ($request->file('categoryPicture')) {
            $file = $request->file('categoryPicture');
            $file->move('images', $file->getClientOriginalName());

            $category->image = $file->getClientOriginalName();
        }

        $category->category = $request->get('categoryName');
        $category->description = $request->get('categoryDescription');
        
        $category->save();

        return redirect('/');
       
    }

    public function delete(Request $request) 
    {   
        $file = Card::where('id', $request->card_id)->pluck('image')->first();        

        $image_path = public_path('images/'. $file);

        Card::destroy($request->card_id);

        if($image_path == 1){
            unlink($image_path);
        }       

        return back();
    }

    // lection methods

    public function lection(LectionRequest $request) 
    {

        $input = $request->all();

        $lection = new Lection;
       
        $lection->title = $request->get('lectionTitle');       
        $lection->description = $request->get('lectionDescription');  
        $lection->card_id = $request->get('lectionSelect');  
        $lection->date = $request->get('lectionDate');  
        $lection->save();

        $session = \Session::flash('successLection');

        return back();
    }

    public function addLection() 
    {

        $cards = Card::with('lections')->get();

        return view('actions.add-lection', compact('cards'));
    }

    public function lectionEdit($id) 
    {

        $cards = Card::with('lections')->get();
        $lection = Lection::where('id', $id)->first();

        if(!$lection) {
            return redirect("/");
        }
        
        return view('edit.lection-edit', compact('cards', 'lection'));
    }
    // *** lectionUpdate e zakomentirano bidejki vo baranjeto e naglaseno samo brishenje na lekcija ***

    // public function lectionUpdate(EditLectionRequest $request, $id)
    // {            
    //     $lection = Lection::where('id', $id)->first();            
    //     $lection->title = $request->get('lectionTitle');
    //     $lection->description = $request->get('lectionDescription');
    //     $lection->card_id = $request->get('categorySelect');

    //     if(!$lection->date = $request->get('lectionDate')) {

    //         $lection->date = $request->get('hiddenDate');
         
    //     } else {
    //         $lection->date = $request->get('lectionDate');  
    //     }       
    
    //     $lection->save();  

    //     $session = \Session::get('id');
        
    //     return redirect('category/page/'. $session);               
    // }
    
    public function deleteLection(Request $request) 
    {    
        Lection::destroy($request->lection_id); 

        return back();
    }
    
    // banner methods

    public function banner(BannerRequest $request)
    {

        $input = $request->all();
        
        $banner = new Banner;
        
        $banner->title = $request->get('bannerName');       
        $banner->text = $request->get('bannerDescription');  
        $banner->link = $request->get('bannerLink');  
        $banner->save();

        $session = \Session::flash('successBanner');
        
        return back();
    }

    public function addBanner() 
    {

        $cards = Card::with('lections')->get();

        return view('actions.add-banner', compact('cards'));
    }
   
    public function bannerEdit($id) 
    {

        $cards = Card::with('lections')->get();
        $banner = Banner::where('id', $id)->first();
        
        if(!$banner) {
            return redirect("/");
        }

        return view('edit.banner-edit', compact('cards', 'banner'));
    }

    public function bannerUpdate(EditBannerRequest $request, $id) 
    {

        $banner = Banner::where('id', $id)->first();

        $banner->title = $request->get('bannerTitle');
        $banner->text = $request->get('bannerText');
        $banner->link = $request->get('bannerLink');

        $banner->save(); 
        
        $session = \Session::get('id');

        return redirect('category/page/'. $session);
        
    }

    public function deleteBanner(Request $request) 
    {          
        Banner::destroy($request->banner_id);        
        return back();
    }
    
    public function showVisitors() {

        $cards = Card::get('category');  
        $visitors = Visitor::get();        

        return view('show-visitors', compact('visitors', 'cards'));
    }
}
