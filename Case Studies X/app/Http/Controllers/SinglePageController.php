<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Visitor;
use App\Email;
use App\Card;
use App\Lection;
use App\Banner;

use Illuminate\Http\Request;

class SinglePageController extends Controller
{
    public function index($id)
    {  
    $cards = Card::with('lections')->get(['id', 'category']);
    
    $category = Card::where('id', $id)->first();  
    
    $lections = Lection::where('card_id', $id)->get(); 
    
    $banners = Banner::get();
    
    \Session::put('id', $category->id);
        

    return view('category-page', compact('lections', 'cards', 'category', 'banners'));
    }
    

    public function registerVisitor(Request $request) 
    {   
      $input = $request->all();
      
      $rules = [
      'emailVisitor' => 'required|email|unique:visitors,email,NULL,NULL,category_id,'.$input['hidden'],
      'hidden' => 'unique:visitors,email,NULL,NULL,category_id,'.$input['emailVisitor'],
        
      ];      

      $messages = [
      'emailVisitor.required' => 'Полето е задолжително.',
      'emailVisitor.email' => 'Внесовте невалиден формат.',         
      'emailVisitor.unique' => 'Внесената адреса е веќе регистрирана на оваа категорија.',         
      ];
      
      $validator = Validator::make($input, $rules, $messages);
      
      if ($validator->fails()) {    
      return response()->json($validator->messages());          
      }
      
      $visitor = new Visitor(); 
      $visitor->email = $input['emailVisitor'];
      $visitor->category_id= $input['hidden'];         
      $visitor->save();

      return response()->json(['success' => $visitor]); 
    }
}
