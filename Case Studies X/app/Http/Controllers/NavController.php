<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Visitor;

use Illuminate\Http\Request;

class NavController extends Controller
{
    public function register(Request $request) 
    {   
      $input = $request->all();   

      $rules = [
         'email' => 'required|email|unique:visitors,email,NULL,NULL,category_id,'.$input['select'],
         'select' => 'required|unique:visitors,category_id,NULL,NULL,email,'.$input['email'],
      ];      

      $messages = [
         'email.required' => ' Полето е задолжително.',
         'email.email' => 'Внесовте невалиден формат.', 
         'email.unique' => 'Оваа адреса е искористена за дадената категорија.',       
         'select.required' => 'Полето е задолжително.',
         'select.unique' => 'Оваа категорија е искористена за дадената адреса.'
      ];
      
      $validator = Validator::make($input, $rules, $messages);

      if ($validator->fails()) {    
         return response()->json($validator->messages());          
      }
      
      $visitor = new Visitor();        
      $visitor->email = $input['email'];
      $visitor->category_id = $input['select'];  
      $visitor->save();

      return response()->json(['success' => $visitor]); 
   }
}
