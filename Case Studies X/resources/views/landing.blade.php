@extends('layouts.app')

@section('custom')
    <style>
        .back {
            display:none;
        }
    </style>
@endsection

@section('input')
<div class="container margin-bottom">
    <div class="row">
        <div class="input-container col-md-12">          
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="input-title">
                        <strong>Приклучи се на 1350 ентузијасти и учи дигитални вештини. Бесплатно.</strong>
                    </h4>
                    <div class="text-success text-left message success" id="success-email" style="display:none">
                        <strong>Вашите податоци се успешно испратени.</strong>
                    </div> 
                    <form class="input-group mb-3" method="POST" id="sign-up-form" action="{{ route('email') }}" autocomplete="off">
                        @csrf
                        <input type="text" class="form-control email-input" name="emailOnly"  id="emailOnly" placeholder="Е-адреса">                                      
                        <div class="input-group-append">
                            <button class="btn email-btn btn-sm" type="submit" id="sign-up">Пријави се</button>
                        </div>
                    </form>            
                    <div class="text-danger text-left message error" id="emailOnly-alert" style="display:none"></div>              
                    <a href="#" class="text-white input-link">можеш да се исклучиш од листата на маилови во секое време!</a>  
                </div>
                <div class="col-md-3"></div>
            </div>                
        </div>
    </div>
</div>  
@endsection

@section('content')
<div class="container">
    
    @if(count($cards) <= 0)  
        <div class="row">
            <div class="card my-card col-md">
                <div class="card-body">
                    <h4 class="card-title text-center text-muted">                            
                        <strong>Во моментов немаме достапни категории, ве молиме обидете се подоцна.</strong>                            
                    </h4>                               
                </div>
            </div>             
        </div>                
        @else
        <div class="row margin-auto">
        @foreach($cards as $card)
            <div class="col-sm-6 col-12 col-md-4">
                <div class="card my-thumbnail">
                    <div class="picture text-center linked">
                        <a href="{{ url('category/page', $card->id) }}">
                            <img src="images/{{$card->image}}" alt="image" class="img-fluid thumbnail-image img-thumbnail">
                        </a>
                    </div>
                    <div class="card-body linked">
                        <a href="{{ url('category/page', $card->id) }}">
                            <h5><strong>{{ $card->category }}</strong></h5>
                        </a>
                        <p class="description card-text">{{ $card->description }}</p>         
                        <div class="text-center counter">
                            <strong class="text-muted number-lections">{{ $card->lections->count() }} @if($card->lections->count() > 1 || $card->lections->count() == 0) лекции @else лекција @endif </strong>   
                        </div> 
                        @if(Auth::check())
                            @if(Auth::user()->role == 'admin')
                            <div class="admin-buttons text-center">
                                <a type="submit" href="{{ route('categoryEdit', $card->id) }}" class="btn category-btn my-btn text-white">
                                    <strong>Edit <i class="far fa-edit"></i></strong>
                                </a>
                                <button data-toggle="modal" data-target="#deleteModal" data-id="{{ $card->id }}" class="btn  category-btn my-btn text-white delete">
                                    <strong>Delete <i class="far fa-trash-alt"></i></strong>
                                </button>   
                            </div> 
                            @endif
                        @endif               
                    </div>
                </div>
            </div>
        @endforeach 
    @endif   
    </div>
</div>     


<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal">Дали сте сигурни дека сакате да ја избришете оваа категорија?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body admin-modal">
        <button type="button" class="btn btn-secondary modal-btn" data-dismiss="modal">Не</button>
        <form action="{{ route('delete') }}"  method="POST">
            <input type="hidden" name="card_id" id="card_id" value="">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn my-btn modal-btn text-white">Да</button>
        </form>        
      </div>      
    </div>
  </div>
</div>  
@endsection

@section('script')
<script>  
    $(document).ready(function() {  
        
        $(document).on("click", ".delete", function () {            
            let myCatId = $(this).data('id');            
            $(".modal-body #card_id").val( myCatId );            
        });
        
        $(document).on('click', '#sign-up', function(e) {
            e.preventDefault();
                        
            $.ajax({
                url:'api/email/register',
                method:'POST', 
                data: {
                    'emailOnly': $('#emailOnly').val(),                                           
                }                                      
            }).done(function(data){ 
                if(data.success)  {                    
                    $('#success-email').show();
                    $('#sign-up-form').find('input:text').val(''); 
                    $('#emailOnly-alert').hide();                    
                }  else {
                    $('#success-email').hide();

                    if(data.emailOnly) {
                        $('#emailOnly-alert').show().text(data.emailOnly);
                    } else {
                        $('#emailOnly-alert').hide();
                    }                   
                }                
            }); 
        })  
    });
</script>   
@endsection
