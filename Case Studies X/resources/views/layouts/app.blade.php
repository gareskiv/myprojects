<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Case Studies X') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/c633cc817e.js" crossorigin="anonymous"></script>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">    
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    @hasSection('custom')
        @yield('custom')
    @endif
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{ route('landing') }}">
                <img src="{{ URL::asset('images/logo.svg') }}" class="logo-img">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto topnav">
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="https://brainster.co/courses?type=academy">Академии</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Вебинари</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Тест за кариера</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"  target="_blank" href="https://blog.brainster.co/">Блог</a>
                    </li>  
                    @if(Auth::check())
                        @if(Auth::user()->role == 'admin')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="adminDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Акции
                            </a>
                            <div class="dropdown-menu" aria-labelledby="adminDropdown">
                                <a class="dropdown-item" href="{{ route('visitors') }}">Посетители</a>
                                <a class="dropdown-item" href="{{ route('addCategory') }}">Додај категорија</a>
                                <a class="dropdown-item" href="{{ route('addLection') }}">Додај лекција</a>
                                <a class="dropdown-item" href="{{ route('addBanner') }}">Додај банер</a>
                            </div>                            
                        </li>   
                        @endif
                    @endif                                     
                    <li class="nav-item btn-item">
                        <a class="btn nav-btn text-white" type="button" href="#" data-toggle="modal" data-target="#popUp">Пријави се</a>                                                      
                    </li>    
                
                    @auth
                        <li class="nav-item dropdown logout-item">
                            <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a> -->

                            <!-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> -->
                                <a class="btn logout-btn" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Одјави се') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            <!-- </div> -->
                        </li>
                    @endauth  
                     
                </ul>
            </div>

            <!-- The Modal -->
            <div class="modal fade" id="popUp" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content my-modal">
                        <div class="modal-header">                           
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="text-white" id="close">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body"> 
                            <div class="container">
                                <div class="row modal-row">
                                    <h3 class="input-title text-center col-md-12">
                                    <strong>Пријави се и учи дигитални вештини. Бесплатно.</strong>
                                    </h3>                  
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <div class="text-success message"  id="success-alert" style="display:none">                                       
                                            <div>Вашите податоци се успешно испратени.</div>
                                        </div>   
                                        <form action="{{ route('register') }}" method="POST" autocomplete="off" id="subscribe-form">
                                            @csrf
                                            <input class="form-control modal-form email-input" type="text" placeholder="Е-адреса" name="emailTest" id="email">
                                            <div class="text-danger message" id="email-alert" style="display:none"></div>  
                                            @if(!isset($category->id)) 
                                            <select class="form-control modal-form modal-select" name="select" id="select">  
                                                <option value="">Изберете категорија</option>                                    
                                                @foreach($cards as $category)                                   
                                                <option value="{{ $category->id }}">{{ $category->category }}</option>
                                                @endforeach
                                            </select>
                                            <div class="text-danger message" id="select-alert" style="display:none"></div> 
                                            @else
                                            <input type="hidden" id="select" name="select" value="{{ $category->id }}">
                                            @endif                                                                                          
                                            <div class="text-center btn-div">
                                                <button type="submit" class="btn email-btn email-modal-btn btn-sm" id="log-in">Пријави се</button>
                                            </div>
                                        </form>           
                                    </div>   
                                    </div class="col-md-2"></div>       
                                </div>
                            </div>                                 
                        </div>                        
                    </div>
                </div>
            </div>

        </nav>
        <main class="py-4">
            @hasSection('input')
                @yield('input')
            @endif
            @yield('content')
        </main>
    </div> 
    <script>  
        $(document).ready(function() {     
            $(document).on('click', '#log-in', function(e) {
                e.preventDefault();
                $.ajax({
                    url:'/api/visitor/register',
                    method:'POST', 
                    data: {
                        'email': $('#email').val(),
                        'select': $('#select').val(),                                                  
                    }                                      
                }).done(function(data){                     
                    if(data.success)  {                    
                        $('#success-alert').show();
                        $('#subscribe-form')[0].reset();
                        $('#email-alert').hide();
                        $('#select-alert').hide();
                    }  else {
                        $('#success-alert').hide();

                        if(data.email) {
                            $('#email-alert').show().text(data.email);
                        } else {
                            $('#email-alert').hide();
                        }

                        if(data.select) {
                            $('#select-alert').show().text(data.select);
                        } else {
                            $('#select-alert').hide();
                        }                     
                    }                
                }); 
            })     

            $(document).on('hide.bs.modal', '#popUp', function() {            
                $('#email-alert').hide();
                $('#select-alert').hide();
                $('#success-alert').hide();
                $('#subscribe-form')[0].reset();
            });
        });
    </script>   
    @hasSection('script')
        @yield('script')
    @endif
</body>
</html>
