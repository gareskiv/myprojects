@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        @if (Session::get('successCategory'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Категоријата е успешно додадена!</strong>
            </div>                  
        @endif     
            <form method="POST" action="{{route('category')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="categoryName">Име на категорија:</label>
                    <input type="text" class="form-control" id="categoryName" placeholder="Име на категорија" name="categoryName">       
                    @error('categoryName')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror                
                </div>
                <div class="form-group">
                    <label for="categoryDescription">Опис на категорија:</label>
                    <textarea type="text" class="form-control" id="categoryDescription" rows="4" placeholder="Опис на категорија" name="categoryDescription"></textarea> 
                    @error('categoryDescription')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>
                <div class="form-group">
                    <label for="categoryPicture" class="categoryPicture">Слика:</label>
                    <br />                        
                    <input type="file" id="categoryPicture" accept="image/*" name="categoryPicture">  
                    @error('categoryPicture')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror          
                </div>
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-md add-btn my-btn text-white">Додај</button>
                </div>
                
            </form>  
        </div>
    </div>     
</div>
@endsection