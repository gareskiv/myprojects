@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        @if (Session::get('successLection'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Лекцијата е успешно додадена!</strong>
            </div>                  
        @endif     
            <form method="POST" autocomplete="off" action="{{ route('lection') }}">
                @csrf
                <div class="form-group">
                    <label for="lectionTitle">Наслов на лекцијата:</label>
                    <input type="text" class="form-control" id="lectionTitle" placeholder="Наслов на лекцијата" name="lectionTitle">       
                    @error('lectionTitle')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror                
                </div>
                <div class="form-group">
                    <label for="lectionDescription">Опис на лекцијата:</label>
                    <textarea type="text" class="form-control" id="lectionDescription" placeholder="Опис на лекцијата" rows="4" name="lectionDescription"></textarea> 
                    @error('lectionDescription')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>              
                <label for="lectionSelect">Категорија на која припаѓа оваа лекција:</label>
                <select class="form-control" name="lectionSelect" id="lectionSelect">  
                    <option value="">Изберете категорија</option>                                    
                    @foreach($cards as $category)                                   
                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                    @endforeach
                </select>
                @error('lectionSelect')
                    <div class="text-danger">{{ $message }}</div>
                @enderror  <br/>
                <div class="form-group">
                    <label for="lectionDate">Избери дата:</label><br>
                    <input type="date" id="lectionDate" placeholder="Дата на лекцијата" name="lectionDate"> 
                    @error('lectionDate')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>                          
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-md my-btn add-btn text-white">Додај</button>
                </div>
                
            </form>  
        </div>
    </div>     
</div>
@endsection