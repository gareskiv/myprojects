@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        @if (Session::get('successBanner'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Банерот е успешно додаден!</strong>
            </div>                  
        @endif     
            <form method="POST" action="{{ route('banner') }}" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="bannerName">Име на банерот:</label>
                    <input type="text" class="form-control" id="bannerName" placeholder="Име на банерот" name="bannerName">       
                    @error('bannerName')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror                
                </div>
                <div class="form-group">
                    <label for="bannerDescription">Опис на банерот:</label>
                    <textarea type="text" class="form-control" id="bannerDescription" placeholder="Опис на банерот" name="bannerDescription" rows="4"></textarea>
                    @error('bannerDescription')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>
                <div class="form-group">
                    <label for="bannerLink">Линк на банерот:</label>
                    <input type="text" class="form-control" id="bannerLink" placeholder="Линк за банерот" name="bannerLink"> 
                    @error('bannerLink')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>
                
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-md my-btn add-btn text-white">Додај</button>
                </div>
                
            </form>  
        </div>
    </div>     
</div>
@endsection