@extends ('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 form-group">
            <label for="search">Пребарувај:</label>            
            <input type="search"  class="form-control mb-2" id="search" placeholder="Внеси имејл или категорија..." autocomplete="off">
        </div>
        <div class="col-md-12 table-responsive">
        <table class="col-md-12 table">
            <thead>
                <tr>                   
                    <th scope="col">Имејл</th>
                    <th scope="col">Категорија</th>                
                </tr>
            </thead>
            <tbody id="user-table">
            @foreach($visitors as $visitor)
                <tr>
                    <td>{{$visitor->email}}</td>
                    @if($visitor->category_id == null)
                        <td> / </td>
                    @else
                        <td>{{$visitor->category->category}}</td>        
                    @endif        
                </tr>  
            @endforeach             
            </tbody>
        </table>
        </div>
    </div>
</div>
    @section('script')
    <script>
        $(document).ready(function(){
            
            $('#search').on('keyup', function() {
                let value = $(this).val().toLowerCase();

                $('#user-table tr').filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    @endsection
@endsection


