@extends('layouts.app')

@section('input')
<div class="container margin-bottom">   
    <div class="row">
        <div class="input-container col-md-12">          
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h4 class="input-title">
                        <strong>Приклучи се на 1350 ентузијасти и учи дигитални вештини. Бесплатно.</strong>
                    </h4>
                    <div class="text-success text-left message success" id="success-email" style="display:none">
                        <strong>Вашите податоци се успешно испратени.</strong>
                    </div> 
                    <form class="input-group mb-3" method="POST" id="sign-up-form" action="{{ route('registerVisitor') }}" autocomplete="off">
                        @csrf
                        <input type="text" class="form-control email-input" name="emailVisitor"  id="emailVisitor" placeholder="Е-адреса">                                      
                        <div class="input-group-append">
                            <button class="btn email-btn btn-sm" type="submit" id="sign-up">Пријави се</button>
                        </div>                      
                            <input type="hidden" id="hidden" name="hidden" value="{{ $category->id }}">                                                    
                    </form>            
                    <div class="text-danger text-left message error" id="emailVisitor-alert" style="display:none"></div>              
                    <a href="#" class="text-white input-link">можеш да се исклучиш од листата на маилови во секое време!</a>  
                </div>
                <div class="col-md-3"></div>
            </div>                
        </div>
    </div>
</div>  
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8 col-7">
            @if(count($lections) <= 0)       
                <div class="card my-card">
                    <div class="card-body">
                        <h4 class="card-title text-center lection-title text-muted">                            
                            <strong>Нема достапни лекции во оваа категорија.</strong>                            
                        </h4>                               
                    </div>
                </div>                     
            @else
            @foreach($lections as $lection)               
                    <div class="card my-card">
                        <div class="card-body">
                            <h4 class="card-title lection-title">
                                <span class="text-muted"> #{{$lection->id}}</span> 
                                <strong>{{$lection->title}}</strong>
                                <small class="class=p-3 mb-2 bg-warning-color">
                                    <b>{{ \Carbon\Carbon::parse($lection->date)->format('F d, Y')}}</b>
                                </small>
                            </h4>
                            <p class="card-text lection-scroll lection-text">{{$lection->description}}</p>  
                            @if(Auth::check())
                                @if(Auth::user()->role == 'admin')
                                <div class="admin-buttons text-left">
                                    <!-- Zakomentirano poradi baranje vo specifikacija samo za delete opcija :)
                                    <a type="submit" href="{{ route('lectionEdit', $lection->id) }}" class="btn my-btn text-white">
                                        <strong>Edit <i class="far fa-edit"></i></strong>
                                    </a> -->
                                    <button data-toggle="modal" data-target="#deleteLection" data-id="{{ $lection->id }}" class="btn my-btn text-white lection">
                                        <strong>Delete <i class="far fa-trash-alt"></i></strong>
                                    </button>   
                                </div> 
                                @endif
                             @endif                               
                        </div>
                    </div>                
            @endforeach
            @endif
            </div>        
            <div class="col-md-3 col-sm-4 col-5">
            @if(count($banners) <= 0)
                <div class="card my-card">
                    <div class="card-body">
                        <h5 class="card-title blink banner-title"><strong>Место за вашата реклама!</strong></h5>
                        <p><strong>Контакт:</strong> <a href="#">contact@brainster.co</a></p>                       
                    </div>
                </div>
            @else 
                @foreach($banners as $banner)
                    <div class="card my-card">
                        <div class="card-body">
                            <h4 class="card-title banner-title"><strong>{{$banner->title}}</strong></h4>                        
                            <p class="card-text banner-scroll banner-text">{{$banner->text}}</p>   
                            <div class="banner-div">
                                <a href="{{ $banner->link }}"  target="blank_" class="card-link card-link-banner"> 
                                    <div class="banner-link-text"> Повеќе </div> 
                                    <i class="fas fa-arrow-right"></i>
                                </a>
                            </div>
                            @if(Auth::check())
                                @if(Auth::user()->role == 'admin')
                                <div class="admin-buttons text-center">
                                    <a type="submit" href="{{ route('bannerEdit', $banner->id) }}" class="btn banner-btn my-btn text-white">
                                        <strong>Edit <i class="far fa-edit"></i></strong>
                                    </a>
                                    <button data-toggle="modal" data-target="#deleteBanner" data-id="{{ $banner->id }}" class="btn banner-btn my-btn text-white banner">
                                        <strong>Delete <i class="far fa-trash-alt"></i></strong>
                                    </button>   
                                </div> 
                                @endif
                             @endif                             
                        </div>
                    </div>
                @endforeach
            @endif
            </div>
        </div>    
    </div>

    <!-- Lection Modal -->
    <div class="modal fade" id="deleteLection" tabindex="-1" role="dialog" aria-labelledby="lectionModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="lectionModal">Дали сте сигурни дека сакате да ја избришете оваа лекција?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body admin-modal">
                <button type="button" class="btn btn-secondary modal-btn" data-dismiss="modal">Не</button>
                <form action="{{route('deleteLection', $lection->id ?? '')}}"  method="POST">
                    <input type="hidden" name="lection_id" id="lection_id" value="">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn my-btn modal-btn text-white">Да</button>
                </form>        
            </div>      
            </div>
        </div>
    </div>  

    <!-- Banner Modal -->
    <div class="modal fade" id="deleteBanner" tabindex="-1" role="dialog" aria-labelledby="bannerModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bannerModal">Дали сте сигурни дека сакате да ја избришете овој банер?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body admin-modal">
                <button type="button" class="btn btn-secondary modal-btn" data-dismiss="modal">Не</button>
                <form action="{{route('deleteBanner', $banner->id ?? '')}}"  method="POST">
                    <input type="hidden" name="banner_id" id="banner_id" value="">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn my-btn modal-btn text-white">Да</button>
                </form>        
            </div>      
            </div>
        </div>
    </div>     
@endsection

@section('script')
<script>  
    $(document).ready(function() { 

        $(document).on("click", ".lection", function () {            
            let myLectId = $(this).data('id');
            $(".modal-body #lection_id").val( myLectId );            
        });

        $(document).on("click", ".banner", function () {             
            let myBannerId = $(this).data('id');      
            $(".modal-body #banner_id").val( myBannerId );            
        });

        function blink_text() {
            $('.blink').fadeOut(700);
            $('.blink').fadeIn(700);
        }
        setInterval(blink_text, 1000);      
        
        $(document).on('click', '#sign-up', function(e) {
            e.preventDefault();
            
            $.ajax({
                url:'/api/single/visitor/register',
                method:'POST', 
                data: {
                    'emailVisitor': $('#emailVisitor').val(),
                    'hidden': $('#hidden').val()                                 
                }                                      
            }).done(function(data){ 
                if(data.success)  {                    
                    $('#success-email').show();
                    $('#sign-up-form').find('input:text').val(''); 
                    $('#emailVisitor-alert').hide();                    
                }  else {
                    $('#success-email').hide();

                    if(data.emailVisitor) {
                        $('#emailVisitor-alert').show().text(data.emailVisitor);
                    } else {
                        $('#emailVisitor-alert').hide();
                    }                   
                }                
            }); 
        })  
    });
</script>
@endsection