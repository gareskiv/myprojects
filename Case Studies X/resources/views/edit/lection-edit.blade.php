@extends('layouts.app')

@section('content')
<div class="container">            
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{route('lectionUpdate', $lection->id) }}" autocomplete="off">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="lectionTitle">Наслов на лекцијата:</label>
                    <input type="text" class="form-control" id="lectionTitle" placeholder="Наслов на лекцијата" name="lectionTitle" value="@if(old('lectionTitle') == '') {{ $lection->title }} @else {{ old('lectionTitle') }} @endif">       
                    @error('lectionTitle')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror                
                </div>
                <div class="form-group">
                    <label for="lectionDescription">Опис на лекцијата:</label>
                    <textarea rows="4" type="text" class="form-control" id="lectionDescription" placeholder="Опис на лекцијата" name="lectionDescription"  value="">@if(old('lectionDescription') == ''){{$lection->description }}@else{{old('lectionDescription')}}@endif</textarea> 
                    @error('lectionDescription')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>
                <div class="form-group">
                    <label for="categorySelect" class="">Избери категорија:</label>                                             
                    <select id="categorySelect" class="form-control" name="categorySelect"  value="">                                               
                        @foreach($cards as $category)                                   
                            <option value="{{ $category->id }}" @php echo $category->id === $lection->card_id ? 'selected' : '' @endphp>{{ $category->category }}</option>
                        @endforeach                            
                    </select> 
                    @error('CategorySelect')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>   
                <div class="form-group">
                    <label for="lectionDate">Избери дата:</label>
                    <input type="hidden" id="hiddenDate" name="hiddenDate" value="{{$lection->date}}">
                    <input type="date"  id="lectionDate" placeholder="ММ/ДД/ГГ" name="lectionDate" value=""> 
                    @error('lectionDate')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>                                            
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-md  add-btn my-btn text-white">Измени</button>
                </div>                        
            </form>  
        </div>
    </div>
</div>    
@endsection