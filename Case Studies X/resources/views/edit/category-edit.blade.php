@extends('layouts.app')

@section('content')
<div class="container">           
    <div class="row">
        <div class="col-md-12">
            <form method="POST" autocomplete="off" action="{{ route('categoryUpdate', $singleCard->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="categoryName">Име на категорија:</label>
                    <input type="text" class="form-control" id="categoryName" placeholder="Име на категорија" name="categoryName" value="@if(old('categoryName') == '') {{ $singleCard->category }} @else {{ old('categoryName') }} @endif">       
                    @error('categoryName')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror                
                </div>
                <div class="form-group">
                    <label for="categoryDescription">Опис на категорија:</label>
                    <textarea rows="4" type="text" class="form-control" id="categoryDescription" placeholder="Опис на категорија" name="categoryDescription"  value="">@if(old('categoryDescription') == ''){{$singleCard->description}}@else{{old('categoryDescription')}}@endif</textarea> 
                    @error('categoryDescription')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>
                <div class="form-group">
                    <label for="categoryPicture" class="categoryPicture">Слика:</label>
                    <br />                        
                    <input type="file" id="categoryPicture" accept="image/*" name="categoryPicture"  value="@if(old('categoryPicture') == '') {{ $singleCard->image }} @else {{ old('categoryPicture') }} @endif">  
                    @error('categoryPicture')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror          
                </div>
                <div class="form-group text-left">
                    <button type="submit" class="btn add-btn btn-md my-btn text-white">Измени</button>
                </div>                        
            </form>  
        </div>
    </div>
</div>        
@endsection