@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="bannerTitle">Наслов на банерот:</label>
                    <input type="text" class="form-control" id="bannerTitle" placeholder="Наслов на банерот" name="bannerTitle" value="@if(old('bannerTitle') == '') {{ $banner->title }} @else {{ old('bannerTitle') }} @endif">       
                    @error('bannerTitle')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror                
                </div>
                <div class="form-group">
                    <label for="bannerText">Опис на банерот:</label>
                    <textarea rows="4" type="text" class="form-control" id="bannerText" placeholder="Опис на банерот" name="bannerText"  value="">@if(old('bannerText') == ''){{$banner->text}}@else{{old('bannerText')}}@endif</textarea> 
                    @error('bannerText')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>
                <div class="form-group">
                    <label for="bannerLink" class="">Линк:</label>                                             
                    <input type="text" id="bannerLubk" class="form-control" name="bannerLink"  value="@if(old('bannerLink') == '') {{ $banner->link }} @else {{ old('bannerLink') }} @endif">            
                    @error('bannerLink')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror           
                </div>                        
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-md  add-btn my-btn text-white">Измени</button>
                </div>                        
            </form>  
        </div>
    </div>
</div>
@endsection