<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategoryIdToVisitors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visitors', function (Blueprint $table) {
            $table->bigInteger('category_id')->unsigned()->nullable()->after('email');
            $table->foreign('category_id','fk_category_id')->references('id')->on('cards')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unique(['email', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visitors', function (Blueprint $table) {
            $table->dropForeign('fk_category_id');
            $table->dropUnique(['email', 'category_id']);
            $table->dropColumn('category_id');
        });
    }
}
