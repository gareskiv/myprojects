<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCardIdToLections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lections', function (Blueprint $table) {
            $table->bigInteger('card_id')->unsigned()->after('id');
            $table->foreign('card_id','fk_card_id')->references('id')->on('cards')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lections', function (Blueprint $table) {
            $table->dropForeign('fk_card_id');
            $table->dropColumn('card_id');
        });
    }
}
