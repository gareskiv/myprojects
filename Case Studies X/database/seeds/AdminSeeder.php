<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'email' => 'admin@example.com',
            'name' => 'Admin',
            'password' => \Hash::make('admin'),
            'role' => 'admin',

        ]);
    }
}
