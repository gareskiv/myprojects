<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->insert([
            ['category' => 'Маркетинг'],
            ['category' => 'Бизнис'],
            ['category' => 'UX'],
            ['category' => 'Data Science'],
            ['category' => 'Програмирање'],
            ['category' => 'Дизајн'],
        ]);
    }
}
