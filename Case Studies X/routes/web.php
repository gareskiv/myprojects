<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'LandingPageController@index')->name('landing');
Route::get('category/page/{id}', 'SinglePageController@index')->name('single');


Auth::routes(['register' => false]);

Route::group(['middleware' => 'role'], function()
{
    Route::get('/admin/panel', 'HomeController@index')->name('home');
    Route::get('visitors', 'AdminController@showVisitors')->name('visitors');

    Route::get('category/edit/{id}', 'AdminController@categoryEdit')->name('categoryEdit');
    Route::get('lection/edit/{id}', 'AdminController@lectionEdit')->name('lectionEdit');
    Route::get('banner/edit/{id}', 'AdminController@bannerEdit')->name('bannerEdit');

    Route::get('add/category', 'AdminController@addCategory')->name('addCategory');
    Route::get('add/lection', 'AdminController@addLection')->name('addLection');
    Route::get('add/banner', 'AdminController@addBanner')->name('addBanner');

    Route::post('add/category', 'AdminController@category')->name('category');
    Route::post('add/lection', 'AdminController@lection')->name('lection');
    Route::post('add/banner', 'AdminController@banner')->name('banner');

    Route::put('category/edit/{id}', 'AdminController@categoryUpdate')->name('categoryUpdate');
    // dokolku ima potreba od edit opcija za lekcija samo izbrishi comment :) 
    // Route::put('lection/edit/{id}', 'AdminController@lectionUpdate')->name('lectionUpdate');
    Route::put('banner/edit/{id}', 'AdminController@bannerUpdate')->name('bannerUpdate');

    Route::delete('/', 'AdminController@delete')->name('delete');
    Route::delete('delete/{id}', 'AdminController@deleteLection')->name('deleteLection');
    Route::delete('category/page{id}', 'AdminController@deleteBanner')->name('deleteBanner');
   
});

