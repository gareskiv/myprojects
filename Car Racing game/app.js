$(function() {
	$(window).on('click', function() {
		$('.finished-bg').css('display', 'none');	
	});

	$('#race-btn').on('click', function(e) {
		e.preventDefault();
		$('.countdown-bg').css('display', 'block');        
		let counterInt = 3;
		$('#counter').html(counterInt);
		counterInt--;
        let intervalCounter = setInterval(function() {
			$('#counter').html(counterInt);
			counterInt--;
            if(counterInt === 0){
				clearInterval(intervalCounter);			
				setTimeout(
					function() {
						$('.countdown-bg').css('display', 'none');
						race();				
					}
				,1000);
			}
		}, 1000);		
		$('#start-over-btn').prop('disabled', true);
		$('#race-btn').prop('disabled', true);		
	});	

	let complete = false;
	let place = 'first';
	let previous = false;

	previuosResults();

	function race() {
		let carWidth = $('.car').width();
		let raceTrackWidth = $(window).width() - carWidth;		
		let raceTime1 = Math.floor((Math.random() * 5000) + 1);		
		let raceTime2 = Math.floor((Math.random() * 5000) + 1);			

		$('#car-1').animate({
			left: raceTrackWidth + 'px'			
		}, raceTime1, function() {
			checkifComplete();		
			previous = true;	
			let div = $('<div></div>');
			div.addClass(['car-1-paragraph', 'results-paragraph']);
		    div.html('finished in: <span class="firstcar">' + place + '</span> place with a time of: <span class="firstrace">' + raceTime1 + '</span> milliseconds!').css('display', 'block');
			$('.car-1-div').append(div);
			$('#car-1-paragraph .firstcar').addClass('car1');
			$('#car-1-paragraph .firstrace').addClass('car1');		
			let firstCar = $('.car-1-paragraph').last().html();	
			localStorage.setItem('car1', firstCar);
		});

		$('#car-2').animate({
			left: raceTrackWidth + 'px'			
		}, raceTime2, function() {
			checkifComplete();
			previous = true;	
			let div = $('<div></div>');
			div.addClass(['car-2-paragraph', 'results-paragraph']);
			div.html('finished in: <span class="secondcar">' + place + '</span> place with a time of: <span class="secondrace">' + raceTime2 + '</span> milliseconds!').css('display', 'block');
			$('.car-2-div').append(div);
			$('#car-2-paragraph .secondcar').addClass('car2');
			$('#car-2-paragraph .secondrace').addClass('car2');		
			let secondCar = $('.car-2-paragraph').last().html();	
			localStorage.setItem('car2', secondCar);
		});
	};

	function checkifComplete() {		
		if(complete == false) {
			complete = true;
			$('.finished-bg').css('display', 'block');				
		} else {
			place = 'second';
			$('#start-over-btn').prop('disabled', false);
			$('#race-btn').attr({
				'data-toggle': 'tooltip',
				'data-placement': 'bottom',
				'title': "This button is disabled for now."
			});
		}
	};

	function reset() {
		complete = false;
		place = 'first';
		$('#race-btn').removeAttr('title', 'data-toggle', 'data-placement');
	};

	function previuosResults() {		
		if (localStorage.getItem('car1') && localStorage.getItem('car2')) {
			$('#results-1').append('<span class="firstcar">Car1</span> ' + localStorage.getItem('car1'));
			$('#results-2').append('<span class="secondcar">Car2</span> ' + localStorage.getItem('car2'));
			$('.show-previous').css('display', 'block');
		} else {
			$('.show-previous').css('display', 'none');
		}
	};
	
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	  });
    
	$('#start-over-btn').on('click', function() {
		$('.car').css('left', 0);
		$('.show-previous').css('display', 'none');			
		previous = false;		
		$('#race-btn').prop('disabled', false)	
		reset();
	});
	
});