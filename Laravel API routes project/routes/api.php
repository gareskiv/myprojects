<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/insurance',  'InsuranceController@index');
Route::get('/insurance/edit/{id}',  'InsuranceController@show');

Route::post('/insurance/register', 'InsuranceController@register')->name('register');
Route::put('/insurance/update/{id}',  'InsuranceController@update')->name('update');
Route::delete('/insurance/{id}',  'InsuranceController@destroy')->name('destroy');
