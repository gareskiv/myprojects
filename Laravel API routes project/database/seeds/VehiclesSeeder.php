<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class VehiclesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = (new \Faker\Factory())::create();
        for ($i = 0; $i < 25; $i++) {
            $faker->addProvider(new \Faker\Provider\Fakecar($faker));
            $v = $faker->vehicleArray;
            $from_date = strtotime(date('Y-m-d', strtotime("-2 years")));
            $to_date = strtotime(date('Y-m-d', strtotime("2 years")));
            $random_date = rand($from_date, $to_date);          
            DB::table('vehicles')->insert([
                'brand' => $v['brand'],
                'model' => $v['model'],
                'plate_number' => $faker->vehicleRegistration,
                'insurance_date' => date('Y-m-d', $random_date),                
            ]);
        }
        
    }
}
