<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Vehicle;

class InsuranceController extends Controller
{   
    public function register(Request $request) 
    {   $input = $request->all();

        $validator = \Validator::make($input, [
            'brand' => 'required',
            'model' => 'required',
            'plate_number' => 'required',
            'insurance_date' => ['required', 'regex:/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/'],           
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(), 200);          
        }
        
        $vehicle = new Vehicle();        
        $vehicle->brand = $input['brand'];
        $vehicle->model = $input['model'];       
        $vehicle->plate_number = $input['plate_number'];       
        $vehicle->insurance_date = $input['insurance_date'];         
        $vehicle->save();

        return response()->json(['success' => $vehicle]); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle = Vehicle::get();
        $data=[];

        foreach ($vehicle as $vehicle) {
           $data[] = [
               'id' => $vehicle->id,
               'model' => $vehicle->model,
               'brand' => $vehicle->brand,
               'plate_number'=> $vehicle->plate_number,
               'insurance_date' => $vehicle->insurance_date,              
               'deleted_at' => $vehicle->deleted_at,
               'redirect_to' => url('/edit/' . $vehicle->id)
            ];
        }

        return response()->json(['vehicles' => $data]);
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::where('id', $id)->get();
        $data=[];
      
        foreach($vehicle as $vehicle) {
            $data = [
                'id' => $vehicle->id,
                'model' => $vehicle->model,
                'brand' => $vehicle->brand,
                'plate_number'=> $vehicle->plate_number,
                'insurance_date' => $vehicle->insurance_date,                        
            ];
        }

        return response()->json(['vehicle' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $validator = \Validator::make($request->all(), [
            'brand' => 'required',
            'model' => 'required',
            'plate_number' => 'required',
            'insurance_date' => ['required', 'regex:/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/'],            
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages(),200);          
        } 

        $vehicle = Vehicle::where('id', $id)->first();
       
        $vehicle->model = $request->get('model');
        $vehicle->brand = $request->get('brand');
        $vehicle->plate_number = $request->get('plate_number');
        $vehicle->insurance_date = $request->get('insurance_date');       
        $vehicle->save();

        return response()->json(['success' => $vehicle]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        try {
          $vehicle = Vehicle::find($id);
          $vehicle->delete($id);      

            return response()->json(['success' => true]);
        } catch(Exception $e) {
            return response()->json(['success' => false]);
        }
    }
    
}
