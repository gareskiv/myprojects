<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;

class ViewController extends Controller
{
  public function index() {
    return view('dasboard');
  }

  public function register() {
    return view('register');
  }

  public function show($id) {
    $identificator = Vehicle::where('id', $id)->first();
    
    if(!$identificator){
      return redirect('/');
    }
    return view('edit', ['id'=> $id, 'identificator' => $identificator]);
  }

}
