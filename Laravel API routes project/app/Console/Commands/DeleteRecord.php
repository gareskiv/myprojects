<?php

namespace App\Console\Commands;

use App\Vehicle;

use Illuminate\Console\Command;

class DeleteRecord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'record:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes soft deleted and expired records from database permanently in every 15 minutes.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $vehicle = \DB::table('vehicles')->WhereNotNull('deleted_at')->orWhere('insurance_date', '<', date('Y-m-d', time()))->delete();      
    }
}
