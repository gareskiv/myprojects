@extends('layouts.master')

@section('content')
<div class="buttons">
    <a class="btn btn-dark btn-sm home" href="{{route('home') }}">Home Page</a>  
    <a class="btn btn-primary btn-sm register" href="{{ route('register') }}">Register Vehicle</a>  
</div> 
<form class="edit-form">           
    <div class="form-group">
        <label for="brand">Brand</label>
        <input type="text" class="form-control" id="brand" name="brand">
        <div class="text-danger" id="brand-alert" style="display:none"></div>       
    </div>
    <div class="form-group">
        <label for="model">Model</label>
        <input type="text" class="form-control" id="model" name="model"> 
        <div class="text-danger" id="model-alert" style="display:none"></div>   
    </div>
    <div class="form-group">
        <label for="plate">Plate Number</label>
        <input type="text" class="form-control" id="plate" name="plate_number">
        <div class="text-danger" id="plate-alert" style="display:none"></div>   
    </div>
    <div class="form-group">
        <label for="insurance">Insurance Date</label>
        <input type="text" class="form-control" id="insurance" name="insurance_date" placeholder="Y-M-D">
        <div class="text-danger" id="insurance-alert" style="display:none"></div>   
    </div>   
    <button class="btn btn-sm btn-primary register-btn">Register</button>
</form>
@endsection

@section('script')
<script>
    $(document).on('click', '.register-btn', function(e) {
            e.preventDefault();
            
            $.ajax({
                url:'api/insurance/register',
                method:'POST', 
                data: {
                    'brand': $('#brand').val(),
                    'model': $('#model').val(),
                    'plate_number': $('#plate').val(),
                    'insurance_date': $('#insurance').val(),                    
                }   
                            
            }).done(function(data){ 
                if(data.success)  {
                    window.location.href = '/';
                }  else {
                    if(data.brand) {
                        $('#brand-alert').show().text(data.brand[0]);
                    } else {
                        $('#brand-alert').hide();
                    }

                    if(data.model) {
                        $('#model-alert').show().text(data.model[0]);
                    } else {
                        $('#model-alert').hide();
                    }
                    
                    if(data.plate_number) {
                        $('#plate-alert').show().text(data.plate_number[0]);
                    } else  {
                        $('#plate-alert').hide();                    
                    }
                    
                    if(data.insurance_date) {
                        $('#insurance-alert').show().text(data.insurance_date[0]);
                    } else {
                        $('#insurance-alert').hide();
                    }                      
                }                
            }); 
        })         
</script>
@endsection