@extends('layouts.master')

@section('content')
<div class="buttons">
    <a class="btn btn-dark btn-sm home" href="{{ route('home') }}">Home Page</a>  
    <a class="btn btn-primary btn-sm register" href="{{ route('register') }}">Register Vehicle</a>  
</div> 
<form class="edit-form">                    
    <div class="form-group">
        <label for="brand">Brand</label>
        <input type="text" class="form-control" id="brand" name="brand" value="">
        <div class="text-danger" id="brand-alert" style="display:none"></div>   
    </div>
    <div class="form-group">
        <label for="model">Model</label>
        <input type="text" class="form-control" id="model" name="model" value="">  
        <div class="text-danger" id="model-alert" style="display:none"></div>   
    </div>
    <div class="form-group">
        <label for="plate">Plate Number</label>
        <input type="text" class="form-control" id="plate" name="plate_number" value=""> 
        <div class="text-danger" id="plate-alert" style="display:none"></div>   
    </div>
    <div class="form-group">
        <label for="insurance">Insurance Date</label>
        <input type="text" class="form-control" id="insurance" name="insurance_date" value="" placeholder="Y-M-D">      
        <div class="text-danger" id="insurance-alert" style="display:none"></div>   
    </div>   
    <input type="hidden" name="id" id="id" value="{{$id}}">    
    <button class="btn btn-sm btn-primary update-btn">Update</button>
</form>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        let id = $('#id').val();
        $.ajax({
                url:'/api/insurance/edit/' + id,
                method:'GET',                
        }).done(function(data){            
             $('#brand').attr('value', data.vehicle.brand);
             $('#model').attr('value', data.vehicle.model);
             $('#plate').attr('value', data.vehicle.plate_number);
             $('#insurance').attr('value', data.vehicle.insurance_date);          
        })
    })

    $(document).on('click', '.update-btn', function(e){
        e.preventDefault();

        let id = $('#id').val();

        $.ajax({
            url:'/api/insurance/update/' + id,
            method:'PUT',
            data: {                                              
                'brand': $('#brand').val(),
                'model': $('#model').val(),
                'plate_number': $('#plate').val(),
                'insurance_date': $('#insurance').val(),                
            }    

        }).done(function(data){
            if(data.success) {
                window.location.href = '/';
            } else {
                if(data.brand) {
                    $('#brand-alert').show().text(data.brand[0]);
                } else {
                    $('#brand-alert').hide();
                }

                if(data.model) {
                    $('#model-alert').show().text(data.model[0]);
                } else {
                    $('#model-alert').hide();
                }
                
                if(data.plate_number) {
                    $('#plate-alert').show().text(data.plate_number[0]);
                } else  {
                    $('#plate-alert').hide();                    
                }
                
                if(data.insurance_date) {
                    $('#insurance-alert').show().text(data.insurance_date[0]);
                } else {
                    $('#insurance-alert').hide();
                }                 
            }                     
        })
    })
</script>
@endsection