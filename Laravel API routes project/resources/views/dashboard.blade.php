@extends('layouts.master')

@section('content')
<h1 class="text-center text-primary insurance-title ">Insurance Dashboard</h1>  
<div class="row">
    <div class="col-md-12">
        <div class="buttons">
            <a class="btn btn-dark btn-sm home" href="{{ route('home') }}">Home Page</a>  
            <a class="btn btn-primary btn-sm register" href="{{ route('register') }}">Register Vehicle</a>  
        </div> 
    </div>
    <div class="col-md-12" id="main-column">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Brand</th>
                    <th scope="col">Model</th>
                    <th scope="col">Plate Number</th>
                    <th scope="col">Insurance Date</th>            
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>   
       
</div> 
@endsection

@section('script')
<script>
    $(document).ready(function(){     

        myFunction();    

        $(document).on('click', '.remove', function(e) {
            e.preventDefault();
            let id = $(this).data('id');

            let parent =$(this).parent().parent();

            $.ajax({
                url:'api/insurance/' + id,
                method:'DELETE',             
            }).done(function(data){  
                parent.fadeOut('linear');                              
            }); 
           
        })          
        
        //    functions

        function myFunction() {            
            $.ajax({
                url:'api/insurance',
                method:'GET',             
            }).done(function(data){              
                for (let i = 0; i < data.vehicles.length; i++) {
                $('tbody').append(`
                    <tr class="table-row">                        
                        <td>${data.vehicles[i].brand}</td>
                        <td>${data.vehicles[i].model}</td>
                        <td>${data.vehicles[i].plate_number}</td>
                        <td>${data.vehicles[i].insurance_date}</td>                          
                        <td> 
                            <a class="btn btn-sm btn-primary edit" data-id="${data.vehicles[i].id}" href="${data.vehicles[i].redirect_to}">Edit Vehicle</a>
                            <button class="btn btn-sm btn-danger remove" data-id="${data.vehicles[i].id}">Delete Vehicle</button>                                      
                        </td>
                    </tr>           
                `);
                }
            }); 
        } 
    })
</script>
@endsection