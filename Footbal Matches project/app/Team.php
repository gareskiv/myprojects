<?php

namespace App;

use App\Match;
use App\Player;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function players() {
        return $this->hasMany(Player::class);
    }

    public function games() {
        return $this->belongsToMany(Match::class, 'team_matches', 'id', 'match_id');
    }
}
