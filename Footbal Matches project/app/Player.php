<?php

namespace App;

use App\Team;
use App\Match;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public function team() {
        return $this->belongsTo(Team::class, 'team_id', 'id');
      }

      public function matches() {
        return $this->belongsToMany(Match::class, 'player_matches', 'player_id', 'id');
    }
}
