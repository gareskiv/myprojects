<?php

namespace App;

use App\Team;
use App\Player;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public function home() {
        return $this->belongsToMany(Team::class, 'team_matches', 'id', 'home_team_id');
    }

    public function away() {
        return $this->belongsToMany(Team::class, 'team_matches', 'id', 'away_team_id');
    }

    public function players() {
        return $this->belongsToMany(Player::class, 'player_matches', 'id', 'match_id');
    }
}
