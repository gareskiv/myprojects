<?php

namespace App\Console\Commands;

use App\Match;
use App\Player;
use App\TeamMatch;
use App\PlayerMatch;

use Illuminate\Console\Command;

class dailyMatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This gives match results for every 24 hours.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $result = Match::whereNull('result')->where('match_date', '>=', date('Y-m-d', strtotime('-1 days')))->where('match_date', '<', date('Y-m-d', time()))->get();
        foreach ($result as $record) {
            $score1 = rand(0, 10);
            $score2 = rand(0, 10);
            $score = $score1 . "-" . $score2;
            Match::where('id', $record->id)->update(['result' => $score]);

            $teamMatch = TeamMatch::where('match_id', $record->id)->first();

            $homePlayers = Player::where('team_id', $teamMatch->home_team_id)->get();
            foreach ($homePlayers as $player) {
                $playerMatch = new PlayerMatch();
                $playerMatch->player_id = $player->id;
                $playerMatch->match_id = $record->id;
                $playerMatch->save();
            }

            $awayPlayers = Player::where('team_id', $teamMatch->away_team_id)->get();
            foreach($awayPlayers as $player) {
                $playerMatch = new PlayerMatch();
                $playerMatch->player_id = $player->id;
                $playerMatch->match_id = $record->id;
                $playerMatch->save();
            }
        }

        return 0;
    }
}
