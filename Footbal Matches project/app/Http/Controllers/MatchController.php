<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\MatchRequest;

use App\TeamMatch;
use App\PlayerMatch;
use App\Team;
use App\Match;

class MatchController extends Controller
{
    public function creatematch() {
        $team = Team::select('id', 'name')->get();

        return view('create-match', ['team' => $team]);
    }
    
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatchRequest $request)
    {
        $input = $request->all();

        if ($input['home'] === $input['away']) {
            $session = \Session::flash('error');
            return back();
        }
        

        $match = new Match();
        $match->match_date = $input['match']; 
        $match->save();       

        $team = new TeamMatch(); 
        $team->match_id = $match['id'];      
        $team->home_team_id = $input['home'];
        $team->away_team_id = $input['away'];
        $team->save(); 
             
        $session = \Session::flash('match');
        return back();            
    }


    public function edit($id)
    {   
        $match = Match::where('id', $id)->first(); 
        $teamMatch = TeamMatch::where('match_id', $id)->first();
        $team = Team::get();

        if(!$match) {
            return redirect('home/admin');
        }

        return view('edit-match', ['match' => $match, 'team' => $team, 'teamMatch' => $teamMatch]);  
       
    }   
    

    public function update(request $request, $id) 
    {    
        $match = Match::where('id', $id)->first();
        $teamMatch = TeamMatch::where('match_id', $id)->first();
        
        $validatedData = $request->validate([
            'result' => ['required', 'regex:/^(10|[0-9])-(10|[0-9])$/'],
            'home' => 'required',
            'away' => 'required',
        ]);
                
        if ($request->get('away') == $request->get('home')) {
            $session = \Session::flash('error');
            return back();
        } else {
            $teamMatch->away_team_id = $request->get('away');
            $teamMatch->home_team_id = $request->get('home');
            $teamMatch->save();

            $match->result = $request->get('result');
            $match->save();

            $session = \Session::flash('success');
          
        }
        
        return back();
    }
    
}
