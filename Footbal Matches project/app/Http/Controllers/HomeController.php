<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateMatchRequest;

use App\Team;
use App\Player;
use App\Match;
use App\TeamMatch;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $played = Match::whereNotNull('result')->get();

            foreach ($played as $match) {
                $result = $match->result;
                $home = $match->home()->first();
                $away = $match->away()->first();

            }

        $pending = Match::whereNull('result')->get();
        
            foreach ($pending as $match) {
                $result = $match->result;
                $home = $match->home()->first();
                $away = $match->away()->first();
            }

        if (\Auth::user()->is_admin) {

           return redirect()->route('admin');
        }  

        return view('home', ['played' => $played, 'pending' => $pending]);
    }

    public function admin()
    {   
        $played = Match::whereNotNull('result')->get();
        
        foreach ($played as $match) {
            $result = $match->result;
            $home = $match->home()->first();
            $away = $match->away()->first();
        }
            

        $pending = Match::whereNull('result')->get();

        foreach ($pending as $match) {
            $result = $match->result;
            $home = $match->home()->first();
            $away = $match->away()->first();
        } 

        return view('home-admin', ['played' => $played, 'pending' => $pending]);        
    }

    public function destroy(Request $request)
    {   
        Match::destroy($request->match_id);
        return back();
    }

}
