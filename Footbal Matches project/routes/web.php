<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['role']], function(){
    Route::get('home/admin',     'HomeController@admin')->name('admin');
    Route::get('create-team',    'TeamController@createTeam')->name('create-team');   
    Route::get('create-player',  'PlayerController@createPlayer')->name('create-player'); 
    Route::get('create-match',   'MatchController@createMatch')->name('create-match'); 
    Route::get('match/{id}',     'MatchController@edit')->name('edit');


    Route::post('create-team',   'TeamController@store')->name('created');
    Route::post('create-player', 'PlayerController@store')->name('created-player');
    Route::post('create-match',  'MatchController@store')->name('create-match');

    Route::delete('home/admin',     'HomeController@destroy')->name('destroy');
    Route::put('match/{id}',        'MatchController@update')->name('update');
    

});


