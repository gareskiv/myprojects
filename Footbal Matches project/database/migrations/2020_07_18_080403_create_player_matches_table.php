<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_matches', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();            
            $table->bigInteger('player_id')->unsigned();  
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');                     
            $table->bigInteger('match_id')->unsigned();  
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_matches');
    }
}
