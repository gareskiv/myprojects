@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="nav">
               @section('nav')
                <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-team') }}">Create Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-player') }}">Add Player</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-match') }}">Schedule a Match</a>
                        </li>                                  
                    </ul>
               @endsection
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-div">
                @if (Session::get('player'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Player has been created successfuly!</strong>
                    </div>
                @endif           
                    <form method="POST" action="{{ route('create-player') }}"  autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="firstname">Firstname:</label>
                            <input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname') }}">   
                            @error('firstname')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror                                  
                        </div>
                        <div class="form-group">
                            <label for="lastname">Lastname:</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}">   
                            @error('lastname')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror                                  
                        </div>
                        <div class="form-group">
                            <label for="birthDate">Birth Date:</label>
                            <input type="text" class="form-control" id="birthDate" name="birthDate"  placeholder="YYYY-MM-DD" value="{{ old('birthDate') }}">
                            @error('birthDate')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror              
                        </div>
                        <div class="form-group">
                            <label for="team">Plays in:</label>
                            <select type="text" class="form-control" id="team" name="team" value="{{ old('team') }}">
                                @foreach($team as $team)
                                    <option value="{{ $team->id }}"> {{ $team->name }}</option>
                                @endforeach
                            </select>
                            @error('team')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror              
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>     
                    </form>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection