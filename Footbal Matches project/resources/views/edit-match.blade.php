@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="nav">
               @section('nav')
                <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-team') }}">Create Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-player') }}">Add Player</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-match') }}">Schedule a Match</a>
                        </li>                                  
                    </ul>
               @endsection
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-div">
                @if (Session::get('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Match updated successfuly.</strong>
                    </div> 
                @endif
                @if (Session::get('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>home team and away team can't be the same.</strong>
                    </div> 
                @endif
                    <form autocomplete="off" method="POST" action="{{ $match->id }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="home">Home Team:</label>
                            <select type="text" class="form-control" id="home" name="home" value="@if(old('home') == '') @else {{ old('home') }} @endif">
                                @foreach($team as $home) <option value="{{$home->id}}" {{ $home->id == $teamMatch->home_team_id ? 'selected' : '' }} >{{ $home->name }}</option> @endforeach
                            </select>
                        @error('home')
                             <div class="text-danger">{{ $message }}</div>
                        @enderror                                  
                        </div>
                        <div class="form-group">
                            <label for="away">Away Team:</label>
                            <select type="text" class="form-control" id="away" name="away" value="@if(old('away') == '') @else {{ old('away') }} @endif">
                                @foreach($team as $away) <option value="{{$away->id}}" {{ $away->id == $teamMatch->away_team_id ? 'selected' : '' }} >{{ $away->name }}</option> @endforeach
                            <select>
                        @error('away')
                             <div class="text-danger">{{ $message }}</div>
                        @enderror                                  
                        </div>
                        <div class="form-group">
                            <label for="result">Result</label>
                            <input type="text" class="form-control" id="result" name="result" value="@if(old('result') == '') {{ $match->result }} @else {{ old('result') }} @endif">   
                        @error('result')
                             <div class="text-danger">{{ $message }}</div>
                        @enderror                                  
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button> 
                    </form>               
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection