@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="nav">
               @section('nav')
                <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-team') }}">Create Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-player') }}">Add Player</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-match') }}">Schedule a Match</a>
                        </li>                                  
                    </ul>
               @endsection
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-div">
                @if (Session::get('team'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Team has been created successfuly!</strong>
                    </div>
                @endif           
                    <form method="POST" action="{{ route('created') }}" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="name">Team Name:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">   
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror                                  
                        </div>
                        <div class="form-group">
                            <label for="year">Year Founded:</label>
                            <input type="text" class="form-control" id="year" name="year"  placeholder="YYYY-MM-DD" value="{{ old('year') }}">
                            @error('year')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror              
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>     
                    </form>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection