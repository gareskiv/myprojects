@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="nav">
               @section('nav')
                <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-team') }}">Create Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-player') }}">Add Player</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create-match') }}">Schedule a Match</a>
                        </li>                                  
                    </ul>
               @endsection
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-div">
                @if (Session::get('match'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Match has been scheduled successfuly!</strong>
                    </div>                  
                @endif          
                @if (Session::get('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>home team and away team can't be the same.</strong>
                    </div> 
                @endif
                    <form method="POST" action="{{ route('create-match') }}" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="match">Match Date:</label>
                            <input type="text" class="form-control" id="match" name="match"  value="{{ old('match') }}" placeholder="YYYY-MM-DD">   
                            @error('match')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror                                  
                        </div>
                        <div class="form-group">
                            <label for="home">Home Team:</label>
                            <select type="text" class="form-control" id="home" name="home" value="{{ old('home') }}">
                                @foreach($team as $home)
                                    <option value="{{ $home->id }}"> {{ $home->name }}</option>
                                @endforeach
                            </select>  
                            @error('home')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror                                  
                        </div> 
                        <div class="form-group">
                            <label for="away">Away Team:</label>
                            <select type="text" class="form-control" id="away" name="away" value="{{ old('away') }}">
                                @foreach($team as $away)
                                    <option value="{{ $away->id }}"> {{ $away->name }}</option>
                                @endforeach
                            </select>
                            @error('away')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror                                  
                        </div>                                    
                        <button type="submit" class="btn btn-primary">Create</button>     
                    </form>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection

@section('js')
    <!-- <script>
        $(document).ready(function(){  
            $("select").change(function() {   
                $("select").not(this).find("option[value="+ $(this).val() + "]").attr('disabled', true);
            }); 
        }); 
    </script> -->
@endsection