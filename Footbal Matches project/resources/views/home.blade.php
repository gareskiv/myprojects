@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
        <div class="col-md-8">
            <h2 class="text-center bold"> All Matches </h2>
            <div class="row">
                <div class="col-md-5 games">
                    <h3 class="text-center match-status"> Played:</h3>
                    <div class="team-identificate">
                        <div class="text-center"><strong>Home</strong></div>                        
                        <div class="text-center"><strong>Away</strong></div>
                    </div>
                    @if (count($played) <= 0)
                        <h3 class="text-muted text-center"> No pending matches.</h3>
                    @endif         
                    @foreach ($played as $match)
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <strong>match date:</strong>
                            <small>{{ $match->match_date }}</small>
                        </div>
                        <hr>
                    </div>     
                        <div class="row">                      
                            <div class="col-md-5 text-center">
                                {{ $match->home->first()->name }} 
                            </div>  
                            <div class="col-md-2 text-center">
                                <strong>{{ $match->result }}</strong>
                            </div>   
                            <div class="col-md-5 text-center">
                                {{ $match->away->first()->name }}
                            </div>                            
                        </div>   
                        <hr>                                    
                    @endforeach                   
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 games">
                    <h3 class="text-center match-status"> Pending:</h3>   
                    <div class="team-identificate">
                        <div class="text-center"><strong>Home</strong></div>                        
                        <div class="text-center"><strong>Away</strong></div>
                    </div>     
                    @if (count($pending) <= 0)
                        <h3 class="text-muted text-center"> No pending matches.</h3>
                    @endif                              
                    @foreach ($pending as $match) 
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <strong>match date:</strong>
                                <small>{{$match->match_date}}</small>
                            </div>
                            <hr>
                        </div>   
                        <div class="row">
                        @if ($match->result == null)     
                            <div class="col-md-5 text-center">
                                {{ $match->home->first()->name }} 
                            </div>  
                            <div class="col-md-2 text-center">
                                <strong> - </strong>
                            </div>   
                            <div class="col-md-5 text-center">
                                {{ $match->away->first()->name }}
                            </div>  
                            @else <h3> There are no pending matches.</h3>
                        @endif  
                        </div>     
                        <hr>                                  
                    @endforeach                                         
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection
