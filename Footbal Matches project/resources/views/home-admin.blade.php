@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="nav">
            @section('nav')
            <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-team') }}">Create Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-player') }}">Add Player</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-match') }}">Schedule a Match</a>
                    </li>                                  
                </ul>
            @endsection
        </div>            
    </div>
    <div class="row">
        <div class="col-md-5 games">
            <h3 class="text-center match-status"> Played:</h3>
            <div class="team-identificate">
                <div class="text-center"><strong>Home</strong></div>                        
                <div class="text-center"><strong>Away</strong></div>
            </div>
             @if (count($played) <= 0)
                <h3 class="text-muted text-center"> No played matches.</h3>
            @endif                
            @foreach ($played as $match)
                <div class="row">
                    <div class="col-md-12 text-center"><strong>match date:</strong><small> {{$match->match_date}} </small></div>
                    <hr>
                </div>              
                <div class="row">                      
                    <div class="col-md-5 text-center">
                        {{ $match->home->first()->name }} 
                    </div>  
                    <div class="col-md-2 text-center">
                        <strong>{{ $match->result }}</strong>
                    </div>   
                    <div class="col-md-5 text-center">
                        {{ $match->away->first()->name }}
                    </div>                            
                </div>   
                <div class="buttons text-center">
                    <a href="{{ route('edit', $match->id) }}" type="button" class="btn btn-sm btn-primary">Edit Match</a> 
                    <form class="form" method="POST" action="{{ route('destroy') }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Delete Match</button>
                        <input type="hidden" name="match_id" value="{{ $match->id }}">
                    </form>
                </div>
                <hr>                      
            @endforeach                   
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5 games">
            <h3 class="text-center match-status"> Pending:</h3>   
            <div class="team-identificate">
                <div class="text-center"><strong>Home</strong></div>                        
                <div class="text-center"><strong>Away</strong></div>
            </div>    
            @if (count($pending) <= 0)
                <h3 class="text-muted text-center"> No pending matches.</h3>
            @endif                                      
            @foreach ($pending as $match)   
                <div class="row">
                    <div class="col-md-12 text-center"><strong>match date:</strong><small> {{$match->match_date}} </small></div>
                    <hr>
                </div>             
                <div class="row">               
                    <div class="col-md-5 text-center">
                        {{ $match->home->first()->name }} 
                    </div>  
                    <div class="col-md-2 text-center">
                        <strong> - </strong>
                    </div>   
                    <div class="col-md-5 text-center">
                        {{ $match->away->first()->name }}
                    </div>                
                </div>    
                <div class="buttons text-center">                   
                    <form class="form" method="POST" action="{{ route('destroy') }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Cancel Match</button>
                        <input type="hidden" name="match_id" value="{{ $match->id }}">
                    </form>
                </div>                
                <hr>                                            
            @endforeach                                         
        </div>
    </div>         
</div>
@endsection
