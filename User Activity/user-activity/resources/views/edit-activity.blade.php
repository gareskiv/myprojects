@extends('layouts.app')

@section('content')
<div class="container">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="{{route('home')}}">User Activities</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('activity-report')}}">Activity Report</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('print-report')}}">Print Report</a>
        </li>
    </ul>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header">{{ __('Edit activity') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="editActivity">
                        <div class="alert alert-success" id="success-msg" role="alert" style="display:none;">
                            <strong>Activity successfully updated.</strong>
                        </div>           
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="date">Date</label>
                                <input type="date" class="form-control" id="date"  name="date" value="{{ old('date', $activity->date) }}" pattern="\d{1,2}/\d{1,2}/\d{4}" required>
                                <div class="text-danger" id="date-error"></div>
                            </div>
                            <div class="form-group col-md-6">
                            <label for="time-spent">Time Spent</label>
                                <input type="number" min="0" max="1440" class="form-control" id="time-spent" placeholder="minutes"  name="minutes" value="{{ old('minutes', $activity->time) }}" required>
                                <div class="text-danger" id="time-spent-error"></div>              
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description">Short Description</label>
                            <textarea rows="4" type="text" class="form-control" id="description" placeholder="Description"  name="description" required> {{ old('description', $activity->description) }}</textarea>
                            <div class="text-danger" id="description-error"></div>
                        </div>
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <input type="hidden" name="id" id="id" value="{{$activity->id}}">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlade')
<script>
    $(document).ready(function(){

        $(document).on('submit', '#editActivity', function(e) {
            e.preventDefault();

            let id = $('#id').val();

            let url = "{{ route('update', ':id') }}";
            url = url.replace(':id', id);

            $.ajax({
                url: url,
                method:'PUT',
                data: {
                    '_token': $('#csrf-token').val(),
                    'date': $('#date').val(),
                    'minutes': $('#time-spent').val(),
                    'description': $('#description').val(),           
                }          
            }).done(function(data){ 
                if(data.response) {
                    $('#date').val(data.response.date);
                    $('#time-spent').val(data.response.time);
                    $('#description').val(data.response.description);

                    $('#date-error').hide();
                    $('#time-spent-error').hide();
                    $('#description-error').hide();                    

                    $('#success-msg').fadeIn(500);
                    setTimeout( function(){$('#success-msg').fadeOut(500)}, 3000);
                    
                }  else {
                    $('#success-msg').fadeOut(500);
                    
                    if(data.date) {
                        $('#date-error').show().text(data.date[0]);
                    } else {
                        $('#date-error').hide();
                    }

                    if(data.minutes) {
                        $('#time-spent-error').show().text(data.minutes[0]);
                    } else {
                        $('#time-spent-error').hide();
                    }
                    
                    if(data.description) {
                        $('#description-error').show().text(data.description[0]);
                    } else  {
                        $('#description-error').hide();                    
                    }
                }                
            }); 
        });
    });    
</script>
@endsection
