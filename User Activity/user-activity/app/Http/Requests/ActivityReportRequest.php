<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ActivityReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateFrom' => 'nullable|date',
            'dateTo' => 'nullable|date|after_or_equal:dateFrom',
        ];
    }

    public function messages() {
        return [
            // 'dateFrom.required' => 'You must specify Date From.',
            'dateFrom.date' => 'Date From must be in valid format.',
            // 'dateTo.required' => 'You must specify Date To.',
            'dateTo.date' => 'Date To must be in valid format.',
            'dateTo.after_or_equal' => 'Date To can not be smaller than Date From.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->messages(), 200));
    }
}
