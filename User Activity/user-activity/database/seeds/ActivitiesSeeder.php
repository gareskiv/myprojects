<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 300;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('activities')->insert([
                'date' => $faker->dateTimeBetween('-10 days', '+3 months'),
                'time' => $faker->numberBetween(1,200),
                'description' => $faker->text(70,200),
                'user_id' => 1, //hardcoded because we can't create new users
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

        }

    }
}
